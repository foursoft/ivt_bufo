<?php

require_once dirname(__FILE__) . '/TextColumn.php';



/**
 * Representation of date data grid column.
 *
 * @author     Roman Sklenář
 * @copyright  Copyright (c) 2009 Roman Sklenář (http://romansklenar.cz)
 * @license    New BSD License
 * @example    http://addons.nette.org/datagrid
 * @package    Nette\Extras\DataGrid
 */
class DateRangeColumn extends DateColumn {
	/** @var string */
	public $format;


	/**
	 * Applies filtering on dataset.
	 * @param  mixed
	 * @return void
	 */
	public function applyFilter($value)
	{
		if (!$this->hasFilter()) return;

		$datagrid = $this->getDataGrid(TRUE);
		$column = $this->getName();
		$cond = array();
		$cond[] = array("[$column] >= %t", $value);
		$datagrid->dataSource->where('%and', $cond);
	}
}