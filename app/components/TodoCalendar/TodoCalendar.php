<?php

	class TodoCalendar extends Control {

		protected $startdate;
		protected $enddate;

		public static $months = array('Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosinec');
		public static $weekdays = array('', 'Pondělí', 'Úterý', 'Středa', 'Čtvrtek', 'Pátek', 'Sobota', 'Neděle');


		public function __construct($parent = NULL, $name = NULL) {
			parent::__construct($parent, $name);

			$this->startdate = date('Y-m-d', strtotime(date('w') == 1 ? 'today' : 'last monday'));
			$this->enddate = date('Y-m-d', strtotime(date('w') == 0 ? 'today' : 'next sunday'));
			
		}

	
		public function render($taskList) {			

			$current = strtotime($this->startdate);
			$startdate = $this->startdate;
			$enddate = $this->enddate;
			
						
			$numDays = $this->dateDiff($this->startdate, $this->enddate);
			
			// create table object
			$table = Html::el('table')->class('DynamicList calendar longterm')->cellspacing(0);
			
			$cells = (object) array('months' => array(), 'weeks' => array(), 'weekdays' => array(), 'days' => array());
			$numOf = (object) array('month' => 1, 'week' => 1, 'day' => 1);
			$last = (object) array('month' => 0, 'week' => 0, 'day' => 0);
			
			// create date bar for each week and attached tasks
			for ($i=0; $i<=$numDays; $i++) {
				
				// add month cells
				if (date('n', $current) != $last->month) {
					$cells->months[] = Html::el('th', self::$months[date('n', $current)-1]);
					$last->month = date('n', $current);
					$numOf->month = 1;
				} else $numOf->month++;
				
				if (!empty($cells->months)) end($cells->months)->colspan($numOf->month);

				// add week cells
				if (date('W', $current) != $last->week) {
					$cells->weeks[] = Html::el('th', date('W', $current).'. týden');
					$last->week = date('W', $current);
					$numOf->week = 1;
				} else $numOf->week++;
				
				if (!empty($cells->weeks)) end($cells->weeks)->colspan($numOf->week);

				// add weekday cells
				$cells->weekdays[] = Html::el('th', self::$weekdays[date('N', $current)]);
				
				// add day cells
				$cells->days[] = Html::el('th', date('d', $current));
				
				// advance to next date
				$current = strtotime('+1 day', $current);
				
			} // endfor
			
			// add all header cells into the table
			foreach($cells as $class => $cellItems) 
				if (!empty($cellItems)) {
					$tr = Html::el('tr')->class($class);
					foreach($cellItems as $i => $td) $tr->add($td->class($this->isEven($i) ? 'even' : ''));
					$table->add($tr);
				} // if empty() / foreach $cells

			
			
			//trace($this->taskList); die;
			
			// add task cells
			$table->add($this->getTaskCells($taskList, strtotime($startdate), $numDays));			
			
			// apply CSS class
			$div = Html::el('div')->class('calendar');
			
			// output table html in <div> wrapper
			print $div->add($table);
			
		}


		/** Generates and returns cells with particular tasks as HTML. */
		protected function getTaskCells($taskList, $start, $numDays) {
		
			$output = Html::el('tr')->class('taskcell');
			$current = $start;
			
			for ($i=0; $i<$numDays; $i++) {
			
				$taskCells = array();
				
				// find all tasks for current day
				foreach($taskList as $task) {
					$tasks = Html::el('ul');
					if ($task->remind == date('Y-m-d', $current)) {

						//$tasks->add(Html::el('li', print_r($task, true))."\n");

						// company
						$tasks->add(Html::el('li', !empty($task->company) ? $task->company : 'IVT')->class('company')."\n");						
						// title
						$tasks->add(Html::el('li', $task->title)->class('title')."\n");
						// salesman
						$tasks->add(Html::el('li', $task->assigned)->class('salesman')."\n");
						
	
						$taskCells[] = Html::el('a')->href('#')->id($task->id)->class($task->status)->add($tasks)->title($task->descr);
					} // endif
					
				} // endforeach
				
				$output->add(Html::el('td')->add(implode("\n", $taskCells)));
				$current = strtotime('+1 day', $current);				
			} // endfor
						
			return $output;			
		}
		
		/** Returns object with dow (day of week), week (number of week) and start (date of 1st day of given week) */
		protected function getWeekStats($date) {
			
			$dow = date('w', $date);
			$week = date('W', $date);
			$start = ($dow != $this->startDay) ? strtotime('last '.$this->daylist[$this->startDay], $date) : $date;
			
			return (object) array('dow' => $dow, 'week' => $week, 'start' => $start);			
		}
		
		/** Returns difference in two dates in days. */
		protected function dateDiff($start, $end) {
			return floor((strtotime($end)-strtotime($start))/(60*60*24));
		}
		
		protected function isEven($num) {
			return ($num % 2 == 0);
		}
	
	
	}
?>