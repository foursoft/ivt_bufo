<?php
	/** ZIP file extractor */
	class ZipHelper {
		
		/** stores extracted files list */
		protected static $cache;
		
	
		/** Extracts $zip file into tmp folder and returns file reference as Array. */
		public static function Extract($zipfile) {

			
			$target = APP_DIR.'/temp/'.substr(sha1(md5(uniqid(rand())).microtime()), 0, 10);
			//$target = sys_get_temp_dir().substr(sha1(md5(uniqid(rand())).microtime()), 0, 10);
			
			// returns empty array if tmp folder cant be created
			if (false === mkdir($target)) return array();
			
			// extract files
			$zip = new ZipArchive();
			$zip->open($zipfile);
			$zip->extractTo($target);
			$zip->close();
			
			return self::FindFiles($target);			
		}


		/** Returns all files in folder as Array. */
		protected static function FindFiles($_location) {
			
			$_location.= (substr($_location, strlen($_location)-1, 1) == '/') ? '' : '/';
			if (false === ($_dir = opendir($_location))) return false;
			$_files = array();
			while (false !== ($_file = readdir($_dir))) if (is_file($_location.$_file)) $_files[] = $_location.$_file;

			return $_files;
		}
		
		
	}

?>