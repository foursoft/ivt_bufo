<?php

	/** Text formatting routines. */
	class FormatHelper {
		
	
		/** Displays empty string if $value is 0. */
		public static function hideEmpty($value) {
			return !empty($value) ? $value : '';
		}
		
		/** Formats $value as integer number. */
		public static function intval($value) {
			return number_format($value, 0, '.', ' ');
		}	

		/** Formats $value as currency */
		public static function currency($value) {
			return !empty($value) ? (self::intval($value).',-') : '';
		}	

		/** Formats date. */
		public static function date($value) {
			return !empty($value) ? date('d. m. Y', strtotime($value)) : '';
		}	

		/** Returns random string of $length characters. If length is not specified or is greater than 40, default length of 40 characters is returned. */
		public static function hash($length = NULL) {
			$hash = sha1(md5(uniqid(rand())) . microtime());
			return ($length !== NULL) ? substr($hash, 0, $length) : $hash;
		}
		

		public static function filename($input) {
			
			$replace = array(
				'ě' => 'e', 'š' => 's', 'č' => 'c', 'ř' => 'r', 'ž' => 'z', 
				'ý' => 'y', 'á' => 'a', 'í' => 'i', 'é' => 'e', 'ú' => 'u', 
				'ů' => 'u', 'ó' => 'o', 'ť' => 't', 'ň' => 'n', 'ď' => 'd',
				'Ě' => 'E', 'Š' => 'S', 'Č' => 'C', 'Ř' => 'R', 'Ž' => 'Z', 
				'Ý' => 'Y', 'Á' => 'A', 'Í' => 'I', 'É' => 'E', 'Ú' => 'U', 
				'Ů' => 'U', 'Ó' => 'O', 'Ť' => 'T', 'Ň' => 'N', 'Ď' => 'D', 
				'(' => '', ')' => '', '[' => '', ']' => '', ' - ' => '-',
				', ' => '-', '.' => '', ' ' => '-', '/' => '-', '\\' => '-',
				':' => '-', '#' => '', '&' => '-', 'ø' => '', '%' => '', '´' => '',
				'!' => '', '°' => '', '"' => ''
				);
				
			return strtolower(strtr(trim($input), $replace));
		}
		
		
		
	}

?>