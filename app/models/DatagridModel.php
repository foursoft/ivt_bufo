<?php

/**
 * Very basic and simple datagrid model example.
 *
 * @author     Roman Sklenář
 * @package    DataGrid\Example
 */
class DatagridModel extends BaseModel
{
    /**
     * Data grid model constructor.
     * @param  string  table name
     * @param  string  primary key name
     * @return void
     */
	public function __construct($table = NULL, $primary = NULL)
	{
		parent::__construct($table, $primary);

		if (!isset($this->primary) && isset($this->table)) {
			try {
				$dbInfo = $this->connection->getDatabaseInfo();
				$this->primary = $dbInfo->getTable($this->table)->getPrimaryKey()->getName();
			} catch(Exception $e) {
				Debug::processException($e);
				throw new InvalidArgumentException("Model must have one primary key.");
			}
		}

		if ($this->connection->profiler) {
			$this->connection->getProfiler()->setFile(Environment::expand('%logDir%') . '/sql.log');
		}
	}


    /**
     * @return DibiConnection
     */
	public function getConnection()
	{
		return $this->connection;
	}


    /**
     * @return array
     */
	public function getTableNames()
	{
		return $this->connection->getDatabaseInfo()->getTableNames();
	}


        public function getTasksInfo()
	{
                return $this->connection->dataSource('SELECT * FROM todo');
	}

	public function getTasks($assign)
	{
            // vypíše all tasks bez jmen společností, jen tabulka todo
            //return $this->connection->dataSource('SELECT * FROM todo WHERE assigned=%s',$assign, 'AND todo.status=%s',"live");
            // vypíše all tasks se jménem společnosti, ale nejsou zahrnuty řádky,kde společnost není uvedena
            //return $this->connection->dataSource('SELECT todo.*,companies.name FROM todo, companies WHERE todo.company = companies.companyID AND assigned=%s',$assign, 'AND todo.status=%s',"live");
            
            // vypíše all tasks i se jménem společnosti, jsou zahrnuty i řádky,kde společnost není uvedena
            return $this->connection->dataSource('SELECT todo.*,companies.name FROM todo LEFT JOIN  companies on todo.company = companies.companyID WHERE assigned=%s',$assign, 'AND todo.status=%s',"live");
        }

	public function getDealersInfo()
	{
		return $this->connection->dataSource(
      'SELECT * FROM users WHERE role="dealer"'
      		);
	}

}