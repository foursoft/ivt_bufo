<?php

	class ProductModel extends BaseModel {
		
		public $table = 'products';


		/** Returns name of a product as an Array */
		public function getProductName($productCodes) {
		  $products = array();
			$result = $this->connection->query("SELECT name FROM products WHERE SHA1(name) IN %l GROUP BY name", explode(',', $productCodes))->fetchAll();
			foreach($result as $product) $products[] = $product->name;
			
			return $products;
		}
		

		/**
		 * @return DibiDataSource
		 */
		public function getDataSource() {
			return $this->connection->dataSource("SELECT id, name, GROUP_CONCAT(DISTINCT code SEPARATOR ', ') AS code, SHA1(name) AS product FROM %n WHERE code != '' GROUP BY name ORDER BY name", $this->table);
			//return $this->connection->dataSource("SELECT id, name, code, id AS product FROM %n WHERE code != '' GROUP BY code ORDER BY name", $this->table);
		}
	
	
		
	}
?>