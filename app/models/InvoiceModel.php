<?php

	class InvoiceModel extends BaseModel {
		
		public $table = 'invoices';


		
		public function getDataSource() {

			$where = array();
			
			// limit display to invoices only
			//$where[] = "I.type = 'issuedInvoice'";
						
			// add company restriction
			if (!empty($this->params['company'])) $where[] = sprintf('C.companyID = %u', $this->params['company']);
			
			//, IF(I.duedate) AS amount2pay
			return $this->connection->dataSource('
				SELECT I.*, IF(CURRENT_DATE() > I.duedate, 1, 0) AS payable, C.name AS companyName
				FROM %n AS I
				JOIN companies AS C ON C.companyID = I.company
				WHERE '.(!empty($where) ? implode(' AND ', $where) : 1).'
				ORDER BY I.issued DESC'
				, $this->table);
				
			return $source;
		}
		
		
		public function getProductSource() {

			$where = array();

			// limit display to invoices only
			//$where[] = "I.type = 'issuedInvoice'";
						
			// add company restriction
			if (!empty($this->params['company'])) $where[] = sprintf('C.companyID = %u', $this->params['company']);
			// add product restriction
			if (!empty($this->params['product'])) $where[] = ("SHA1(T.item) IN %l");
			
			return $this->connection->dataSource('
				SELECT I.*, IF(CURRENT_DATE() > I.duedate, 1, 0) AS payable, C.name AS companyName
				FROM %n AS I
				JOIN companies AS C ON C.companyID = I.company
				JOIN invoice_items AS T ON T.invoice = I.id
				WHERE '.(!empty($where) ? implode(' AND ', $where) : 1)
				
				.' GROUP BY id', $this->table, explode(',', $this->params['product']));
			
		}
		
		/** Returns invoice details & invoice items. */
		public function getInvoiceData($invoiceID) {
			
			$invoiceItems = new InvoiceItemsModel();
			
			$invoiceData = $this->connection->select('%n.*, companies.name AS company', $this->table)->from($this->table)->join('companies ON companies.companyID = %n.company', $this->table)->where('%n=%i', $this->primary, $invoiceID)->fetch();
			$invoiceData->items = $invoiceItems->find(array('invoice' => $invoiceID))->fetchAll();
			
			return $invoiceData;			
		}
		
		/** Finds invoices by $companyID. */
		public function findByCompany($companyID) {
			return $this->setParams(array('company' => $companyID))->getDataSource()->fetchAll();
		}
		
	}
?>