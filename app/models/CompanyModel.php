<?php

	class CompanyModel extends BaseModel {
		
		protected $table = 'companies';
		protected $primary = 'companyID';


		/** Returns name of a company as string */
		public function getCompanyName($company) {
			return $this->connection->query("SELECT name FROM companies WHERE companyID = %i LIMIT 1", $company)->fetchSingle();
		}
		
		
		/** Merges manually added companies with newly imported by company number (ICO). */
		public function mergeAfterImport() {

			return $this->connection->query(		
						'UPDATE `companies` C
						JOIN `companies` M ON (C.ico = M.ico AND M.is_manual)
						LEFT JOIN `product_offers` P ON (P.companyID = M.companyID)
						LEFT JOIN `todo` D ON (D.company = M.companyID)
						SET C.history = M.history, 
						C.phone_proposal = M.phone_proposal, 
						C.trade_proposal = M.trade_proposal,
						C.attachment = M.attachment,
						P.companyID = C.companyID,
						M.deleted = 1
						WHERE NOT C.is_manual');
		}

		/**
		 * @return DibiDataSource
		 */
		public function getDataSource() {
			return $this->connection->dataSource('
			SELECT C.companyID, C.name, C.ico, C.adresCity, GROUP_CONCAT(DISTINCT A.city SEPARATOR %s) AS branchCity, C.contactphone, C.status,
			SUM(IF(I.duedate < CURRENT_DATE(), I.amount2pay, 0)) AS amount2pay,
			MAX(I.issued) AS issued,
			C.attachment
			FROM companies AS C			
			LEFT JOIN invoices AS I ON I.company = C.companyID
			LEFT JOIN shipto_addresses AS A ON A.company = C.companyID
			WHERE NOT deleted
			GROUP BY C.companyID
			', ', ');
		}
	
		/** Returns companies that purchased a product. Param $product is SHA1 encoded product name. */
		public function getProductSource() {
			
			return $this->connection->dataSource("
			SELECT C.companyID, C.name, C.ico, C.adresCity, GROUP_CONCAT(DISTINCT A.city SEPARATOR %s) AS branchCity, C.contactphone, C.status,
			SUM(IF(I.duedate < CURRENT_DATE(), I.amount2pay, 0)) AS amount2pay,
			MAX(I.issued) AS issued,
			C.attachment
			FROM companies AS C			
			LEFT JOIN invoices AS I ON I.company = C.companyID
			LEFT JOIN invoice_items AS T ON T.invoice = I.id
			LEFT JOIN shipto_addresses AS A ON A.company = C.companyID
			WHERE NOT deleted AND SHA1(T.item) IN %l
			GROUP BY C.companyID
			", ', ', explode(',', $this->params['product']));
		}
		
	}

?>