<?php

/**
 * Base abstract model class.
 *
 * @author     Roman Sklenář
 * @package    DataGrid\Example
 */
abstract class BaseModel extends Object implements IModel
{
	/** @var DibiConnection */
	protected $connection;

	/** @var string  table name */
	protected $table;

	/** @var string|array  primary key column name */
	protected $primary = 'id';
	
	/** Params assigned by setParams method */
	protected $params = array();

	/** @var array  of function(IModel $sender) */
	public $onStartup;

	/** @var array  of function(IModel $sender) */
	public $onShutdown;


	public function __construct($table = NULL, $primary = NULL)
	{
		$this->onStartup($this);
		$this->connection = dibi::getConnection();

		if ($table) $this->setTable($table);
		if ($primary) $this->setPrimary($primary);
	}


	public function __destruct()
	{
		$this->onShutdown($this);
	}


	public static function initialize()
	{
    $connection = dibi::connect(Environment::getConfig('database'));
	}


	public static function disconnect()
	{
		dibi::getConnection()->disconnect();
	}


	/***** Public getters and setters *****/


	/** Propagates presenter params to model. Returns fluent BaseModel interface. */
	public function setParams($params) {
		$this->params = $params;
		return $this;
	}

	public function getTable()
	{
		return $this->table;
	}


	public function setTable($table)
	{
		$this->table = $table;
	}


	public function getPrimary()
	{
		return $this->primary;
	}


	public function setPrimary($primary)
	{
		$this->primary = $primary;
	}



	/***** Model's API *****/

    /**
     * @return DibiDataSource
     */
	public function getDataSource() {
		return $this->connection->dataSource("SELECT * FROM %n WHERE 1", $this->table);
	}


	/**
	 * @return DibiFluent
	 */
	public function findAll()
	{
		return $this->connection->select('*')->from($this->table);
	}


	/**
	 * @return DibiFluent
	 */
	 /*
	public function find($id)
	{
		return $this->findAll()->where("%n=%i", $this->primary, $id);
	}
*/
    public function find($id) {
        if (is_array($id)) {
            $query = $this->findAll();
            foreach ($id as $field => $value) {
                $query->where($field . ' = ' . (is_numeric($value) ? '%i' : '%s'), $value);
            }
            return $query;
        } else {
            return $this->findAll()
                    ->where($this->table . '.' . $this->primary . '=%i', $id);
        }
    }

	/**
	 * @return DibiFluent
	 */
	public function update($id, array $data)
	{
		return $this->connection->update($this->table, $data)->where('%n = '.(is_numeric($value) ? '%i' : '%s'), $this->primary, $id)->execute();
	}


	/**
	 * @return DibiFluent
	 */
	public function insert(array $data)
	{
		return $this->connection->insert($this->table, $data)->execute(dibi::IDENTIFIER);
	}

    /** Inserts new items and updates existing ones in a single operation using ON DUPLICATE KEY UPDATE. */
	public function replace($data, $columns = array()) {
		
		if (empty($data)) return;
		
		$multiarray = is_array(current($data));
		$columns = empty($columns) ? array_keys($multiarray ? current($data) : $data) : $columns;		

		$command  = $this->connection->command()->insert();
		
		if ($multiarray) $command->into('%n %ex', $this->table, $data);
		else $command->into('%n (%n)', $this->table, $columns)->values($data);
		
		$values = array();
		foreach($columns as $name) $values[] = "$name = VALUES($name)";
		
		$result = $command->on('DUPLICATE KEY UPDATE %sql', implode(', ', $values))->execute();
		
		return $result;
	}

	/** Truncates the table. */
	public function truncate() {
		return $this->connection->query('TRUNCATE TABLE '.$this->table);
	}

	/**
	 * @return DibiFluent
	 */
	public function delete($id)
	{
		return $this->connection->delete($this->table)->where("%n=%i", $this->primary, $id)->execute();
	}
}