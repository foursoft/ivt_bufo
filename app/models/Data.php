<?php 

class Data extends BaseModel {
	
    /**
     * Shortcut function. It executes a method in a given model class
     * @param string $class
     * @param string $name
     * @return mixed
     */
    public static function factory($class, $name = NULL) {
        $modelName = $class . 'Model';

        if (class_exists($modelName) and (($instance = new $modelName()) instanceof IModel)) {
            $model = $instance;
        } else {
            $model = new Data();
            $model->setTable($class);
        }

        $arguments = func_get_args();
        return $name !== NULL ? call_user_func_array(array($model, $name), array_splice($arguments, 2)) : $model;
    }

	
	
	
}
?>