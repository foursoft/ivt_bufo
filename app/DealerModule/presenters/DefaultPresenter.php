<?php

final class Dealer_DefaultPresenter extends Dealer_BasePresenter {

    public function startup() {
        parent::startup();
        $user = Environment::getUser();

        if (!$user->isAuthenticated()) {
            if ($user->getSignOutReason() === User::INACTIVITY) {
                $this->flashMessage('Uplynula doba expirace přihlášení.', 'warning');
            }

            $backlink = $this->getApplication()->storeRequest();
            $this->redirect('Auth:login', array('backlink' => $backlink));
        } else {
            if (!$user->isAllowed($this->reflection->name, $this->getAction())) {
                $this->flashMessage('Nemáte dostatečná práva na vstup do této sekce!', 'warning');
                $this->redirect('Auth:login');
            }
        }
    }

    public function renderDefault() {
        
				$assigned = Environment::getUser()->getIdentity()->name;

				$startdate = date('Y-m-d', strtotime(date('w') == 1 ? 'today' : 'last monday'));
				$enddate = date('Y-m-d', strtotime(date('w') == 0 ? 'today' : 'next sunday'));
				
				$this->template->today_tasks = dibi::query("
					SELECT C.name AS company, T.todoID AS id, T.title, T.descr, T.whocreate, T.status, T.assigned, T.remind
					FROM todo AS T
					LEFT JOIN companies AS C ON C.companyID = T.company
					WHERE T.remind BETWEEN '$startdate' AND '$enddate'
					ORDER BY T.remind
				")->fetchAll();
				
				//print_r($this->template->today_tasks);

/*
        $this->template->today_tasks = dibi::select('*')
                                        ->from('todo')
                                        ->where('assigned=%s', Environment::getUser()->getIdentity()->name)
                                        ->and('status=%s', "live")
                                        ->and('remind=%s', date("Y-m-j"))
                                        ->fetchAll();
*/
/*     $this->template->expired_tasks = count(dibi::select('todoID')
                                        ->from('todo')
                                        ->where('assigned=%s', Environment::getUser()->getIdentity()->name)
                                        ->and('status=%s', "live")
                                        ->and('remind<%s', date("Y-m-j"))
                                        ->fetchAll());
*/
        $this->template->name = Environment::getUser()->getIdentity()->name;
    }

    public function actionLogout() {
        Environment::getUser()->signOut();
        $this->flashMessage('Jste odhlášen/a.');
        $this->redirect('Auth:login');
    }
		
    protected function createComponentTodoCalendar($name) {
        return new TodoCalendar($this, $name);
		}


/**************** ADD TASK ***************/
    public function addTaskOkClicked(SubmitButton $button) {
        $args = $button->getForm()->getValues();
        //Debug::dump($args);Exit;
        $args['created'] = $today = date('y-m-j h-i-s');
        $args['whocreate'] = Environment::getUser()->getIdentity()->name;
        $args['status'] = 'live';
        $args['assigned'] = Environment::getUser()->getIdentity()->name;
        dibi::insert('todo', $args)->execute();
        $this->redirect('default');
    }
    protected function createComponentAddTaskForm($name) {
        $form = new AppForm($this, $name);
        // All companies list
        $rows = dibi::select('name, companyID')
                        ->from('companies')
                        ->fetchAll();
        //make array from statuses for select
     foreach ($rows as $row) {
            $companies_array[$row['companyID']] = $row['name'];
        }
        $form->addText('remind', 'Remind:')->getControlPrototype()->class('datepicker')
                ->addRule(Form::FILLED, 'Date must be fill');
        $form->addText('title', 'Title:', 100)
                ->addRule(Form::FILLED, 'Title must be fill');
        $form->addTextArea('descr', 'Description', 98, 5);
        
        $form->addSelect('company', 'Společnost:', $companies_array)->skipFirst('');;

        $form->addProtection('Vypršel ochranný časový limit, odešlete prosím formulář ještě jednou');
        $form->addSubmit('ok', 'Add')->onClick[] = array($this, 'addTaskOkClicked');
    }
/**************** ADD TASK ***************/



}
