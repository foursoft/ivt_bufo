<?php

abstract class Dealer_BasePresenter extends BasePresenter
{
	public $oldLayoutMode = FALSE;

	public $oldModuleMode = FALSE;

    protected function beforeRender()
    {
        $user = Environment::getUser();
        $this->template->user = ($user->isAuthenticated()) ? $user->getIdentity() : NULL;
    }

}
