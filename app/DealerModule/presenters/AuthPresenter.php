<?php

final class Dealer_AuthPresenter extends Dealer_BasePresenter
{

    /** @persistent */
    public $backlink = '';

    protected function createComponentLoginForm($name)
    {
        $form = new AppForm($this, $name);

        $form->addText('login', 'Email:')
             ->addRule(Form::EMAIL, 'Zadajte registrační email.');

        $form->addPassword('password', 'Password:')
             ->addRule(Form::FILLED, 'Zadajte heslo.');

        $form->addProtection('Odešlete přihlašovací údaje znova (vypršela platnos tzv. bezpečnostního tokenu).');
        $form->addSubmit('send', 'Log in!');

        $form->onSubmit[] = array($this, 'loginFormSubmitted');
    }

    public function loginFormSubmitted($form)
    {
        try {
            $user = Environment::getUser();
            $user->authenticate($form['login']->value, $form['password']->value);
            $this->getApplication()->restoreRequest($this->backlink);
            $this->redirect('Default:default');
        }
        catch (AuthenticationException $e) {
            $form->addError($e->getMessage());
        }
    }

}
