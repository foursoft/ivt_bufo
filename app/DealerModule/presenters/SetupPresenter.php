<?php

final class Dealer_SetupPresenter extends Dealer_SecuredPresenter {

function renderDefault(){
    $this->template->aaa = Environment::getUser();
    $this->template->username = Environment::getUser()->getIdentity()->name;
    }

    public function okChangePass(SubmitButton $button) {
        $args = $button->getForm()->getValues();
        $config = Environment::getConfig('security');
        $updateValues['password'] = sha1($args['newpass'] . $config->salt);
        dibi::query('UPDATE `users` SET ', $updateValues, 'WHERE `name`=%s', Environment::getUser()->getIdentity()->name);
        $this->redirect('Setup:default');
    }

    protected function createComponentChangePassForm($name) {
        $form = new AppForm($this, $name);
        $form->addPassword('newpass', 'Zadejte své nové heslo:')->addRule(Form::FILLED, 'Enter your new password');
        $form->addProtection('Vypršel ochranný časový limit, odešlete prosím formulář ještě jednou');
        $form->addSubmit('ok', 'Change password')->onClick[] = array($this, 'okChangePass');
    }
}