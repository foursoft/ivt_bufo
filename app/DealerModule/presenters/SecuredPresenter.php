<?php

abstract class Dealer_SecuredPresenter extends Dealer_BasePresenter
{
    public function startup()
    {
        parent::startup();
        $user = Environment::getUser();

        if (!$user->isAuthenticated()) {
            if ($user->getSignOutReason() === User::INACTIVITY) {
                $this->flashMessage('Systém vás z bezpečnostníchh důvodů odhlásil.', 'warning');
            }

            $this->flashMessage('Nejste přihlášen/a!', 'warning');
            $backlink = $this->getApplication()->storeRequest();
            $this->redirect('Auth:login', array('backlink' => $backlink));
        }
        else {
            if (!$user->isAllowed($this->reflection->name, $this->getAction())) {
                $this->flashMessage('Nemáte dostatečná práva na vstup do této sekce!', 'warning');
                $this->redirect('Default:');
            }
        }
    }
}
