<?php

	final class Dealer_InvoicePresenter extends Dealer_BasePresenter {


		protected $invoiceTypes = array('issuedInvoice' => 'faktura', 'issuedCreditNotice' => 'dobropis', 'issuedCorrectiveTax' => 'oprava');

		
		public function renderDefault() {
			 $this->template->subject = ($this->getParam('product') !== NULL) ? (' :: '.($this->getParam('company') !== NULL ? Data::factory('company')->getCompanyName($this->getParam('company')) : '').' <br />'.implode('<br />', Data::factory('product')->getProductName($this->getParam('product')))) : '';
		}
		
		protected function createComponentInvoiceGrid() {

			if ($this->isAjax()) $params = Environment::getSession($this->getName())->params;
			else Environment::getSession($this->getName())->params = $params = $this->getParam();
			
			$productLookup = (isset($params['product']) and $params['product'] !== NULL);
			
			$model = new InvoiceModel();
			$model->setParams($params);

			$grid = new DataGrid();	
			
			$grid->itemsPerPage = 100;
			$grid->rememberState = TRUE;
      $grid->multiOrder = FALSE; // order by one column only
			$grid->bindDataTable($productLookup ? $model->getProductSource() : $model->getDataSource());
	
			$grid->keyName = 'id';
	
			/* add some columns */
			$grid->addColumn('companyName', 'Firma')->getHeaderPrototype()->addStyle('width: 260px');
			$grid->addColumn('type', 'Položka')->getHeaderPrototype()->addStyle('width: 90px');
			$grid->addColumn('variable', 'Var. symbol')->getHeaderPrototype()->addStyle('width: 120px');
			$grid->addDateRangeColumn('issued', 'Vystaveno', '%d. %m %Y')->getHeaderPrototype()->addStyle('width: 110px');
			$grid->addDateRangeColumn('duedate', 'Splatnost', '%d. %m %Y')->getHeaderPrototype()->addStyle('width: 100px');
			$grid->addColumn('pricetotal', 'Suma')->getHeaderPrototype()->addStyle('width: 70px');
			$grid->addColumn('amount2pay', 'Pohled.')->getHeaderPrototype()->addStyle('width: 90px');
			
			$grid['pricetotal']->getCellPrototype()->style('text-align: right;');
			$grid['amount2pay']->getCellPrototype()->style('text-align: right;')->name('amount2pay');

			//$grid->addColumn('product', 'Možnosti');
			$grid->getRenderer()->onRowRender[]  = array($this, 'onRender');
	
	
			/* add some filters */	
			$grid['companyName']->addFilter();
			$grid['variable']->addFilter();
			$grid['issued']->addDateFilter();
			$grid['duedate']->addDateFilter();
			
			$grid['type']->formatCallback[] = array($this, 'onInvoiceType');
			//$grid['issued']->formatCallback[] = 'FormatHelper::date';
			//$grid['duedate']->formatCallback[] = 'FormatHelper::date';
			$grid['pricetotal']->formatCallback[] = 'FormatHelper::currency';
			$grid['amount2pay']->formatCallback[] = 'FormatHelper::currency';
	
			$grid->addActionColumn('Akce')->getHeaderPrototype()->addStyle('width: 50px');
			$icon = Html::el('span');
			
			$grid->addAction('Detail', 'Invoice:detail', clone $icon->class('icon icon-detail'));

			return $grid;
		}		
	
	
	/** Returns name of invoice type */
	public function onInvoiceType($value) {
		return $this->invoiceTypes[$value];
	}
	
	public function onRender(Html $row, DibiRow $data) {
		foreach($row->getIterator() as $td) if ($td->name == 'amount2pay') { $td->class($data->payable ? 'red' : 'green'); }
	}
	
	/** Renders invoice $id detail. */
	public function renderDetail($id) {
		
		$invoiceModel = new InvoiceModel();
		$this->template->invoice = $invoiceModel->getInvoiceData($id);
		
	}

}
?> 