<?php

	final class Dealer_ImportPresenter extends Dealer_BasePresenter {

    protected function createComponentImportForm($name) {

        $form = new AppForm($this, $name);
		
        $form->addGroup('Zdrojový soubor');
        $form->addFile('xmlfile', 'Soubor XML nebo ZIP');

        $form->addGroup(NULL);
       // $form->addSubmit('cancelBtn', 'Zrušit')->setValidationScope(FALSE)->onClick[] = array($this, 'cancelAction');
        $form->addSubmit('submitBtn', 'Importovat');
		
		$form->onSubmit[] = array($this, 'importAction');
	}
	
	protected function isZip(HttpUploadedFile $file) {
		return (false !== strpos($file->getContentType(), 'application/zip'));
	}
	
	public function importAction(AppForm $input) {


            $values = $input->getValues();

            $companyModel = new CompanyModel();
            $shiptoAddressModel = new ShipToAddressModel();
            $statusModel = new StatusModel();
            $invoiceModel = new InvoiceModel();
            $invoiceItemsModel = new InvoiceItemsModel();
            $productModel = new ProductModel();
            
                //print_r($values); die;
            if ($values['xmlfile']->isOk()) {
		
		// prepare source XML file
		$xmlFile = ($this->isZip($values['xmlfile'])) ? current(ZipHelper::Extract($values['xmlfile']->getTemporaryFile())) : $values['xmlfile']->getTemporaryFile();
		
		// instantiate DOM and populate with XML input
		$dom = new DomDocument();
		$dom->load($xmlFile);		
		$xpath = new DOMXPath($dom);
		

		/***  G E N E R A L   I M P O R T  ***/

		$companies = array();
		$shipto_addresses = array();
		$groups = array();
		$invoices = array();
		$invoiceItems = array();
		$products = array();

		$allData = $this->readNodes($xpath->evaluate('//rsp:responsePack/rsp:responsePackItem'));
			
		foreach($allData->responsePackItem->listInvoice->invoice as $invoiceData) {
			
			// skip anything apart from issuedInvoice type
			//if (isset($invoiceData->invoiceHeader->invoiceType) and $invoiceData->invoiceHeader->invoiceType != 'issuedInvoice') continue;

			/***  C O M P A N Y   I M P O R T  ***/			
			$companyID = intval($invoiceData->invoiceHeader->partnerIdentity->id);			
			$addressNode = $invoiceData->invoiceHeader->partnerIdentity->address;
			$shipToAddressNode = isset($invoiceData->invoiceHeader->partnerIdentity->shipToAddress) ? $invoiceData->invoiceHeader->partnerIdentity->shipToAddress : NULL;
			
			// import only companies with ID
			if (!empty($addressNode->ico)) {
			
				$companies[$companyID] = array(
					'companyID' => $companyID,
					'name' => !empty($addressNode->company) ? $addressNode->company : $addressNode->name,
					'adresCity' => $addressNode->city,
					'adresStreet' => $addressNode->street,
					'adresPostalcode' => $addressNode->zip,
					'ico' => $addressNode->ico,
					'contactphone' => !empty($addressNode->phone) ? $addressNode->phone : $addressNode->mobil,
					'fax' => isset($addressNode->fax) ? $addressNode->fax : '',
					'status' => $addressNode->adGroup,
					'notes' => isset($invoiceData->invoiceHeader->note) ? $invoiceData->invoiceHeader->note : ''					
					);
					
				if (!empty($addressNode->adGroup)) $groups[$addressNode->adGroup] = array('statusName' => $addressNode->adGroup);
				
				} // if !empty $addressNode->ico

			if ($shipToAddressNode !== NULL and (is_string($addressNode->zip) and isset($shipToAddressNode->city) and is_string($shipToAddressNode->city)))
			$shipto_addresses[md5((isset($shipToAddressNode->street) ? trim($shipToAddressNode->street) : '').(isset($shipToAddressNode->city) ? trim($shipToAddressNode->city) : ''))] = array(
				'company' => $companyID,
				'name' => isset($shipToAddressNode->company) ? $shipToAddressNode->company : '',
				'city' => isset($shipToAddressNode->city) ? trim($shipToAddressNode->city) : '',
				'street' => isset($shipToAddressNode->street) ? trim($shipToAddressNode->street) : '',
				'zip' => $addressNode->zip
				);
			

			/***  I N V O I C E   I M P O R T  ***/
			$invoiceID = intval($invoiceData->invoiceHeader->id);

			$invoices[$invoiceID] = array(
				'id' => $invoiceData->invoiceHeader->id,
				'type' => $invoiceData->invoiceHeader->invoiceType,
				'variable' => $invoiceData->invoiceHeader->symVar,
				'issued' => $invoiceData->invoiceHeader->date,
				'duedate' => $invoiceData->invoiceHeader->dateDue,
				'company' => $invoiceData->invoiceHeader->partnerIdentity->id,
				'pricetotal' => floor(floatval($invoiceData->invoiceSummary->homeCurrency->priceNone)+floatval($invoiceData->invoiceSummary->homeCurrency->priceLowSum)+floatval($invoiceData->invoiceSummary->homeCurrency->priceHighSum)),
				'amount2pay' => isset($invoiceData->invoiceHeader->liquidation->amountHome) ? floor($invoiceData->invoiceHeader->liquidation->amountHome) : 0,
				'currency' => isset($invoiceData->invoiceSummary->foreignCurrency) ? $invoiceData->invoiceSummary->foreignCurrency->currency->ids : 'CZK',
				'rate' => isset($invoiceData->invoiceSummary->foreignCurrency) ? floatval($invoiceData->invoiceSummary->foreignCurrency->rate) : 1
			);
			
			
			/***  I T E M S   I M P O R T  ***/
			
			// make an array for one-item inovice
			if (!is_array($invoiceData->invoiceDetail->invoiceItem)) $invoiceData->invoiceDetail->invoiceItem = array($invoiceData->invoiceDetail->invoiceItem);
			
			foreach($invoiceData->invoiceDetail->invoiceItem as $item) {

				// import into invoice items
				$invoiceItems[] = array(
					'invoice' => $invoiceID,
					'product' => $item->id,
					'item' => isset($item->text) ? $item->text : '',
					'quantity' => $item->quantity,
					'unitprice' => floatval($item->homeCurrency->unitPrice),
					'totalprice' => floatval($item->homeCurrency->priceSum)
				);
				
				// import into products
				if (!empty($item->text) and intval($item->homeCurrency->priceSum) > 0)
				$products[intval($item->id)] = array(
					'id' => $item->id,
					'name' => $item->text,
					'code' => isset($item->code) ? $item->code : ''
				);
			
			}
						
		} // foreach $invoice

		$companyModel->replace($companies);
		$shiptoAddressModel->replace($shipto_addresses);
		$invoiceModel->replace($invoices);
		$invoiceItemsModel->replace($invoiceItems);
		$productModel->replace($products);
		$statusModel->replace($groups);
                
            } // if file->isOk()
            
                // do merge companies
		$companyModel->mergeAfterImport();

		// go to success page
		$this->redirect('success');
	}
	
	/** Reads DOMNodeList recursively and returns it as object with values. */
	protected function readNodes(DOMNodeList $input) {
		
		$output = array();
		foreach($input as $node) 
			if ($node->nodeType == 1) {
				
				// strip node namespace
				$nodeKey = explode(':', $node->nodeName, 2);
				$nodeName = end($nodeKey);
				
				
/*
				// text node = scalar value
				if ($node->childNodes->length === 1) $output[$nodeName] = (string) $node->nodeValue;
				// read child nodes
				else {
					
					// single node
					if (!isset($output[$nodeName])) $output[$nodeName] = $this->readNodes($node->childNodes);
					// array node
					else {
						// add element into existing array
						if (is_array($output[$nodeName])) $output[$nodeName][] = $this->readNodes($node->childNodes);
						// create array and add initial element and new element
						else $output[$nodeName] = array($output[$nodeName], $this->readNodes($node->childNodes));						
					} // else isset
*/		


				// read child nodes		
				if (intval($node->childNodes->length) > 1) { 

					// single node
					if (!isset($output[$nodeName])) $output[$nodeName] = $this->readNodes($node->childNodes);
					// array node
					else {
						// add element into existing array
						if (is_array($output[$nodeName])) $output[$nodeName][] = $this->readNodes($node->childNodes);
						// create array and add initial element and new element
						else $output[$nodeName] = array($output[$nodeName], $this->readNodes($node->childNodes));						
					} // else isset
				
				} // if childNodes->length
				
				// text node = scalar value
				else $output[$nodeName] = intval($node->childNodes->length) ? $node->nodeValue : '';

				
			} // if $node->nodeType 
				
		return (object) $output;
	}


}
?>