<?php

final class Dealer_LostPresenter extends Dealer_BasePresenter {

    /** @persistent */
    public $backlink = '';

    function generatePassword($length=9, $strength=0) {
        $vowels = 'aeuy';
        $consonants = 'bdghjmnpqrstvz';
        if ($strength & 1) {
            $consonants .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($strength & 2) {
            $vowels .= "AEUY";
        }
        if ($strength & 4) {
            $consonants .= '23456789';
        }
        if ($strength & 8) {
            $consonants .= '@#$%';
        }

        $password = '';
        $alt = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        return $password;
    }

    protected function createComponentLostForm($name) {
        $form = new AppForm($this, $name);

        $form->addText('login', 'Email:')
                ->addRule(Form::EMAIL, 'Zadajte registrační email.');

        $form->addText('spam', '3! = ')
                ->addRule(Form::FILLED, 'Please fill answear for spam question control.');

        $form->addProtection('Odešlete požadavek  znova (vypršela platnos tzv. bezpečnostního tokenu).');
        $form->addSubmit('send', 'Submit');

        $form->onSubmit[] = array($this, 'lostFormSubmitted');
    }

    public function lostFormSubmitted($form) {
        // správná odpověď na SPAM otázku?
        if ($form['spam']->value == "6") {
            $rows = dibi::select('id')
                            ->from('users')
                            ->where('email=%s', $form['login']->value)
                            ->fetchAll();
            // existuje email z formuláře v DB?
            if (!$rows) {
                $this->redirect('Lost:wrongemail');
            } else {
                $newPass = $this->generatePassword();
                $config = Environment::getConfig('security');
                $updateValues['password'] = sha1($newPass . $config->salt);
                dibi::query('UPDATE `users` SET ', $updateValues, 'WHERE `email`=%s', $form['login']->value);
                // odeslani hesla na email
                $mail = new Mail;
                $mail->addTo($form['login']->value);
                $mail->setSubject('Bufo - new password');
                $mail->setBody("New password in Bufo system was genarated. Your new pass is :" . $newPass);
                $mail->send();
                $this->redirect('Lost:good');
            }
        } else {
            $this->redirect('Lost:spam');
        }
    }

}
