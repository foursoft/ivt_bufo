<?php

final class Dealer_TaskPresenter extends Dealer_BasePresenter {

    public function startup() {
        parent::startup();
        $user = Environment::getUser();

        if (!$user->isAuthenticated()) {
            if ($user->getSignOutReason() === User::INACTIVITY) {
                $this->flashMessage('Uplynula doba expirace přihlášení.', 'warning');
            }

            $backlink = $this->getApplication()->storeRequest();
            $this->redirect('Auth:login', array('backlink' => $backlink));
        } else {
            if (!$user->isAllowed($this->reflection->name, $this->getAction())) {
                $this->flashMessage('Nemáte dostatečná práva na vstup do této sekce!', 'warning');
                $this->redirect('Auth:login');
            }
        }
    }
    public function actionLogout() {
        Environment::getUser()->signOut();
        $this->flashMessage('Jste odhlášen/a.');
        $this->redirect('Auth:login');
    }

    protected function createComponentTasksGrid() {
        $assign = Environment::getUser()->getIdentity()->name;

        $model = new DatagridModel;
        $grid = new DataGrid;

        $renderer = new DataGridRenderer;
        $renderer->paginatorFormat = '%input%'; // customize format of paginator
        $grid->setRenderer($renderer);

        $grid->itemsPerPage = 10; // display 20 rows per page
        $grid->displayedItems = array('all', 10, 20, 50); // items per page selectbox items
        $grid->rememberState = TRUE;
        $grid->timeout = '+ 7 days'; // change session expiration after 7 days
        $grid->bindDataTable($model->getTasks($assign));
        $grid->multiOrder = FALSE; // order by one column only
        $grid->keyName = "todoID";

        /*         * ** add some columns *** */

        $grid->addColumn('todoID', 'ID');
        $grid->addColumn('name', 'Firma:');
        $grid->addColumn('title', 'Úkol:');
        $grid->addColumn('descr', 'Popis:');
        $grid->addColumn('remind', 'Datum:');


        /*         * ** add some filters *** */

        $grid['todoID']->addFilter();
        $grid['remind']->addFilter();
        $grid['remind']->addDefaultSorting('asc');

        $grid->addActionColumn('Akce');
        $grid->addAction('Prohlédnout', 'Task:view');
        $grid->addAction('Editovat', 'Task:edit');
        $grid->addAction('Smazat', 'deleteTodo!');
        $grid->addAction('Hotovo', 'doneTodo!');
        return $grid;
    }

    public function renderDefault() {
        $this->template->dnes = date('y-m-j');
        
        }

    public function renderView($todoID) {
        $this->template->todoID = $todoID;
        $todo = dibi::select('*')
                        ->from('todo')
                        ->where('todoID=%i', $todoID)
                        ->and('assigned=%s', Environment::getUser()->getIdentity()->name)
                        ->fetch();
        $this->template->todo = $todo;

    }


    function handleDeleteTodo($todoID) {
        dibi::delete('*')
                ->from('todo')
                ->where('todoID=%i', $todoID)
                ->execute();
    }
    function handleRemoveTodo($todoID) {
        dibi::delete('*')
                ->from('todo')
                ->where('todoID=%i', $todoID)
                ->execute();
        $this->redirect('default');
    }

    function handleDoneTodo($todoID) {
        $updateValues['status'] = "done";
        dibi::query('UPDATE `todo` SET ', $updateValues, 'WHERE `todoID`=%i', $todoID);
    }

    public function renderEdit($todoID) {
        $this->template->todoID = $todoID;
    }

    public function okClicked(SubmitButton $button) {
        $todoID = $this->getParam('todoID');
        $args = $button->getForm()->getValues();
        $updateValues['remind'] = $args[remind];
        $updateValues['title'] = $args[title];
        $updateValues['descr'] = $args[descr];
        dibi::query('UPDATE `todo` SET ', $updateValues, 'WHERE `todoID`=%i', $todoID);
        $this->redirect('view', $todoID);
    }
    public function cancelClicked(SubmitButton $button) {
        // process cancelled
        $this->redirect('view', $todoID = $this->getParam('todoID'));
    }
    protected function createComponentEditTaskForm($name) {
        $todoID = $this->getParam('todoID');
                $todo = dibi::select('*')
                        ->from('todo')
                        ->where('todoID=%i', $todoID)
                        ->fetch();
        $form = new AppForm($this, $name);
        $form->addText('remind', 'Připomenutí (rrrr-mm-dd):')->setDefaultValue($todo->remind)->getControlPrototype()->class('datepicker');
        $form->addText('title', 'Název:')->setDefaultValue($todo->title);
        $form->addTextArea('descr', 'Popis:')->setDefaultValue($todo->descr);
        $form->addProtection('Vypršel ochranný časový limit, odešlete prosím formulář ještě jednou');
        $form->addSubmit('ok', 'Edit')->onClick[] = array($this, 'okClicked');
        $form->addSubmit('cancel', 'Cancel')->setValidationScope(FALSE) // prvek se nebude validovat
                ->onClick[] = array($this, 'cancelClicked');
    }


}
