<?php

final class Dealer_CompaniesPresenter extends Dealer_SecuredPresenter {
	
    public function okClicked(SubmitButton $button) {
        // submitted and valid
        $args = $button->getForm()->getValues();
        //Debug::dump($args);Exit;
        $args['create'] = $today = date('y-m-j h-i-s');
        $args['whocreated'] = Environment::getUser()->getIdentity()->name;
		
		// generate manually created company ID with B prefix (B = BUFO)
		$args['companyID'] = 'B'.rand(0, 9999);
		
		// flag manually added company
		$args['is_manual'] = 1;
	
		dibi::insert('companies', $args)->execute();

		// redirect to newly created company detail
        $this->redirect('detail', array('companyID' => $args['companyID']));
    }

    public function cancelClicked(SubmitButton $button) {
        // process cancelled
        $this->redirect('Default:default');
    }

    public function formSubmitted(AppForm $form) {
        // manual processing
        if (!$form['cancel']->isSubmittedBy()) {
            
        }
    }

    protected function createComponentAddCompanyForm($name) {

        // All statuses from DB
        $rows = dibi::select('statusName')->from('status')->fetchAll();
        //make array from statuses for select
        foreach ($rows as $row) $status[$row['statusName']] = $row['statusName'];

        $country = array('Czech Republic', 'Deutschland', 'Austria', 'Poland', 'Slovakia', 'England');

        $form = new AppForm($this, $name);
        $form->addGroup('Údaje o firmě');
        $form->addSelect('status', 'Subjekt:', $status)
                ->addRule(Form::FILLED, 'Choice status');
        $form->addText('name', 'Firma *')
                ->addRule(Form::FILLED, 'Prosím, uveďte jméno společnosti.');
        $form->addText('ico', 'IČ:')->setOption('description', Html::el('span')->class('ares')->setHtml('<a href="#" target="_blank">Vyhledat na ARESu</a>'));
        $form->addText('adresStreet', 'Ulice *')
                ->addRule(Form::FILLED, 'Prosím, uveďte ulici.');
        $form->addText('adresCity', 'Město *')
                ->addRule(Form::FILLED, 'Prosím, uveďte město.');
        $form->addText('adresPostalcode', 'PSČ *')
                ->addRule(Form::FILLED, 'Prosím, uveďte PSČ.');
        $form->addSelect('country', 'Země *', $country)
                ->addRule(Form::FILLED, 'Prosím, uveďte zemi.');
        $form->addText('companyemail', 'Firemní e-mail:');
        $form->addText('companyphone', 'Firemní telefon:');
        $form->addText('fax', 'Fax');
        $form->addText('web', 'Web');
        $form->addGroup('Kontaktní osoba');
        $form->addText('contactname', 'Jméno *')
                ->addRule(Form::FILLED, 'Prosím, uveďte jméno kontaktní osoby.');
        $form->addText('contactsurname', 'Příjmení *')
                ->addRule(Form::FILLED, 'Prosím, uveďte příjmení kontaktní osoby.');
        $form->addText('contactemail', 'Kontaktní e-mail *')
                ->setEmptyValue('@')
                ->addCondition(Form::FILLED) // conditional rule: if is email filled, ...
                ->addRule(Form::EMAIL, 'Prosím, uveďte e-mailovou adresu ve správném tvaru.');
        $form->addText('contactphone', 'Telefon');
        $form->addText('contactmobile', 'Mobil');
        $form->addText('contactfax', 'Fax');
        $form->addText('contactDescr', 'Poznámka');

        $form->setCurrentGroup(NULL);
        $form->addProtection('Vypršel ochranný časový limit, odešlete prosím formulář ještě jednou');
        $form->addSubmit('cancel', 'Zrušit')->setValidationScope(FALSE) // prvek se nebude validovat
                ->onClick[] = array($this, 'cancelClicked');
        $form->addSubmit('ok', 'Založit firmu')->onClick[] = array($this, 'okClicked');
    }

    
	public function renderDefault() {
			 $this->template->subject = ($this->getParam('product') !== NULL) ? (' :: nákup <br />'.implode('<br />', Data::factory('product')->getProductName($this->getParam('product')))) : '';		
	}
	
	protected function createComponentCompanyGrid() {
		
		if ($this->isAjax()) $params = Environment::getSession($this->getName())->params;
		else Environment::getSession($this->getName())->params = $params = $this->getParam();
		
		$productLookup = (isset($params['product']) and $params['product'] !== NULL);
		
		$model = new CompanyModel();
		$model->setParams($params);
		
		$grid = new DataGrid;

		$renderer = new DataGridRenderer;
		$renderer->paginatorFormat = '%input%'; // customize format of paginator
		$grid->setRenderer($renderer);
		
		$grid->itemsPerPage = 200;
		$grid->displayedItems = array('all', 10, 50, 100, 200); // items per page selectbox items
		// remember settins
	  $grid->rememberState = true;			
		$grid->timeout = '+7 days'; // change session expiration after 7 days
		$grid->bindDataTable($productLookup ? $model->getProductSource() : $model->getDataSource());
		$grid->multiOrder = FALSE; // order by one column only
		

		$grid->keyName = "companyID";

		/* add some columns */
		$grid->addColumn('ico', 'IČ')->getHeaderPrototype()->addStyle('width: 40px');
		$grid->addColumn('name', 'Jméno')->getHeaderPrototype()->addStyle('width: 250px');
		$grid->addColumn('status', 'Subjekt')->getHeaderPrototype()->addStyle('width: 100px');
		$grid->addColumn('adresCity', 'Město')->getHeaderPrototype()->addStyle('width: 190px');        
		$grid->addColumn('branchCity', 'Pobočka')->getHeaderPrototype()->addStyle('width: 190px');        
		$grid->addColumn('contactphone', 'Telefon')->getHeaderPrototype()->addStyle('width: 150px');        
								
		// default sort by company
		//$grid['name']->addDefaultSorting('asc');
		
		$grid['name']->getCellPrototype()->name('company');
		
		if ($productLookup) {
		
			$grid->addDateRangeColumn('issued', 'Nákup', '%d. %m %Y')->getHeaderPrototype()->addStyle('width: 110px');
			$grid['issued']->addDateFilter();
			
		 // default sort by last purchase date
			//$grid['issued']->addDefaultSorting('desc');

		} // if $productLookup
		else {

			$grid->addColumn('amount2pay', 'Pohl.');
			
			$grid['amount2pay']->getHeaderPrototype()->addStyle('width: 110px');
			$grid['amount2pay']->getCellPrototype()->style('text-align: right; color:#FF0000');	
			$grid['amount2pay']->formatCallback[] = 'FormatHelper::hideEmpty';
			$grid['amount2pay']->formatCallback[] = 'FormatHelper::currency';

		} // else $productLookup

		/* add some filters */
		$grid['ico']->addFilter();
		$grid['name']->addFilter();
		$grid['adresCity']->addFilter();
		$grid['branchCity']->addFilter();
		$grid['contactphone']->addFilter();


		$grid->getRenderer()->onRowRender[]  = array($this, 'onRender');


        $rows = dibi::select('statusName')->from('status')->fetchAll();
        foreach ($rows as $row) $statuses[$row['statusName']] = $row['statusName'];
        $grid['status']->addSelectboxFilter($statuses);
        
        $grid->addActionColumn('Akce')->getHeaderPrototype()->addStyle('width: 98px');
        $icon = Html::el('span');
        $grid->addAction('Detail', 'Companies:detail', clone $icon->class('icon icon-detail'));
        $grid->addAction('Faktury', 'companyInvoicing', clone $icon->class('icon icon-invoice')->href('#'), false);
        $grid->addAction('Smazat', 'confirmForm:confirmDelete!', clone $icon->class('icon icon-del'), true);

        return $grid;
    }
	
	public function onRender(Html $row, DibiRow $data) {
		foreach($row->getIterator() as $td) if ($td->name == 'company' && !empty($data->attachment)) $td->class('individual')->title('Individuální ceny');
	}		
	

	/** Handles company invoicing action by redirecting to Invoice presenter and providing arguments. */
	public function actionCompanyInvoicing($companyID) {

		$params = Environment::getSession($this->getName())->params;
				
		$input = array();
		$input['company'] = $companyID;
		
		if (isset($params['product']) and $params['product'] !== NULL) $input['product'] = $params['product'];
		
		$this->redirect('Invoice:default', $input);
	}
	
    /**
     * Custom group operations handler.
     * @param  SubmitButton
     * @return void
     */
    public function renderDetail($companyID) {

		$this->template->companyID = $companyID;
        $row = dibi::select('*')
                        ->from('companies')
                        ->where('companyID=%s', $companyID)
                        ->fetch();
        
		$this->template->company = $row;
		
		$this->template->shipto_addresses = Data::factory('ShipToAddress')->find($companyID)->fetchAll();		
		$this->template->invoices = Data::factory('invoice')->findByCompany($companyID);
		
		$web = $row['web'];
        $http = substr($web, 0, 7);
        if ($http != 'http://') {
            $web = 'http://' . $web;
        }
        $this->template->web = $web;

        $status = dibi::select('*')
                        ->from('status')
                        ->where('statusID=%i', $row->status)
                        ->fetch();
        $this->template->status = $status;

        $tasks = dibi::select('*')
                        ->from('todo')
                        ->where('company=%s', $companyID)
                        ->and('assigned=%s', Environment::getUser()->getIdentity()->name)
                        ->orderBy('remind')
                        ->fetchAll();
        
        $products = dibi::select('P.id, P.name, R.submitted')
                        ->from('product_offers AS R')
						->join('products AS P ON P.id = R.productID')
                        ->where('R.companyID = %s', $companyID)
						->orderBy('P.name')
                        ->fetchAll();

        $this->template->products = $products;
		
		//Debug::dump($tasks);Exit;
        $this->template->tasks = $tasks;
		
		if (!empty($row['attachment'])) {
			
			$file = explode('.', $row['attachment']);
			
			$this->template->download = '<strong><a href="'.$this->link('download', $row['companyID']).'">Stáhnout individuální ceny</a></strong> <br /><em>(formát: '.strtoupper(end($file)).', velikost: '.round(filesize(UPLOAD_DIR.'/'.$row['attachment'])/1000).' kB)</em>';
			
		} else $this->template->download = '<em>Žádná příloha</em>';
		
		$this->template->phone_proposal = $row['phone_proposal'] ? '<p class="info">Firma byla oslovena telefonicky</p>' : '';
		$this->template->trade_proposal = $row['trade_proposal'] ? '<p class="success">Nákup zboží po telefonické nabídce</p>' : '';

        $country = array(
            '', 'Czech Republic', 'Deutschland', 'Austria', 'Poland', 'Slovakia', 'England'
        );
		
        $this->template->country = isset($country[$row['country']]) ? $country[$row['country']] : '';
    }

    public function editCompanyOkClicked(SubmitButton $button) {
        // submitted and valid
        $args = $button->getForm()->getValues();

        $updateValues = NULL;
        if (!$args['name'] == NULL) {
            $updateValues['name'] = $args['name'];
        }
        if (!$args['adresStreet'] == NULL) {
            $updateValues['adresStreet'] = $args['adresStreet'];
        }
        if (!$args['adresCity'] == NULL) {
            $updateValues['adresCity'] = $args['adresCity'];
        }
        if (!$args['adresPostalcode'] == NULL) {
            $updateValues['adresPostalcode'] = $args['adresPostalcode'];
        }
        if (!$args['companyphone'] == NULL) {
            $updateValues['companyphone'] = $args['companyphone'];
        }

        if (!$args['companyemail'] == NULL) {
            $updateValues['companyemail'] = $args['companyemail'];
        }

        if (!$args['country'] == NULL) {
            $updateValues['country'] = $args['country'];
        }

        if (!$args['fax'] == NULL) {
            $updateValues['fax'] = $args['fax'];
        }
        if (!$args['web'] == NULL) {
            $updateValues['web'] = $args['web'];
        }

        //Debug::dump($args);Exit;

        if ($updateValues) {
            dibi::query('UPDATE `companies` SET ', $updateValues, 'WHERE `companyID`=%s', $this->getParam('companyID'));
        }

        $this->redirect('detail', $this->getParam('companyID'));
    }

    protected function createComponentEditCompanyForm($name) {

        $country = array(
            '', 'Czech Republic', 'Deutschland', 'Austria', 'Poland', 'Slovakia', 'England'
        );

                $data = dibi::select('*')
                  ->from('companies')
                  ->where('companyID=%s', $this->getParam('companyID'))
                  ->fetch();

        $form = new AppForm($this, $name);
        $form->addGroup('Informace o společnosti');
        $form->addText('name', 'Firma:')->setDefaultValue($data['name']);
        $form->addText('adresStreet', 'Ulice:')->setDefaultValue($data['adresStreet']);
        $form->addText('adresCity', 'Město:')->setDefaultValue($data['adresCity']);
        $form->addText('adresPostalcode', 'PSČ:')->setDefaultValue($data['adresPostalcode']);
        $form->addSelect('country', 'Země:', $country)->setDefaultValue($data['country']);
        $form->addText('companyemail', 'Email:')->setDefaultValue($data['companyemail']);
        $form->addText('companyphone', 'Telefon:')->setDefaultValue($data['companyphone']);
        $form->addText('fax', 'Fax:')->setDefaultValue($data['fax']);
        $form->addText('web', 'Web:')->setDefaultValue($data['web']);
        $form->setCurrentGroup(NULL);
        $form->addProtection('Vypršel ochranný časový limit, odešlete prosím formulář ještě jednou');
        $form->addSubmit('ok', 'Odeslat')->onClick[] = array($this, 'editCompanyOkClicked');
    }

    public function editContactPersonOkClicked(SubmitButton $button) {
        $args = $button->getForm()->getValues();
        $updateValues = NULL;
        if (!$args['contactname'] == NULL) {
            $updateValues['contactname'] = $args['contactname'];
        }
        if (!$args['contactsurname'] == NULL) {
            $updateValues['contactsurname'] = $args['contactsurname'];
        }
        if (!$args['contactemail'] == NULL) {
            $updateValues['contactemail'] = $args['contactemail'];
        }
        if (!$args['contactphone'] == NULL) {
            $updateValues['contactphone'] = $args['contactphone'];
        }
        if (!$args['contactmobile'] == NULL) {
            $updateValues['contactmobile'] = $args['contactmobile'];
        }
        if (!$args['contactfax'] == NULL) {
            $updateValues['contactfax'] = $args['contactfax'];
        }
        if (!$args['contactDescr'] == NULL) {
            $updateValues['contactDescr'] = $args['contactDescr'];
        }
        //Debug::dump($args);Exit;
        if ($updateValues) {
            dibi::query('UPDATE `companies` SET ', $updateValues, 'WHERE `companyID`=%s', $this->getParam('companyID'));
        }
        $this->redirect('detail', $this->getParam('companyID'));
    }

    protected function createComponentEditContactPersonForm($name) {
        $form = new AppForm($this, $name);

        $data = dibi::select('*')
                  ->from('companies')
                  ->where('companyID=%s', $this->getParam('companyID'))
                  ->fetch();

        $form->addGroup('Kontaktní osoba');
        $form->addText('contactname', 'Jméno:')->setDefaultValue($data['contactname']);
        $form->addText('contactsurname', 'Příjmení:')->setDefaultValue($data['contactsurname']);
        $form->addText('contactemail', 'E-mail:')->setDefaultValue($data['contactemail']);
        $form->addText('contactphone', 'Telefon:')->setDefaultValue($data['contactphone']);
        $form->addText('contactmobile', 'Mobil:')->setDefaultValue($data['contactmobile']);
        $form->addText('contactfax', 'Fax:')->setDefaultValue($data['contactfax']);
        $form->addText('contactDescr', 'Popis:')->setDefaultValue($data['contactDescr']);
        $form->setCurrentGroup(NULL);
        $form->addProtection('Vypršel ochranný časový limit, odešlete prosím formulář ještě jednou');
        $form->addSubmit('ok', 'Odeslat')->onClick[] = array($this, 'editContactPersonOkClicked');
    }


    protected function createComponentBusinessForm($name) {
        
		$form = new AppForm($this, $name);
		
		$data = dibi::select('attachment, phone_proposal, trade_proposal')
					->from('companies')
					->where('companyID=%s', $this->getParam('companyID'))
					->fetch();

        $form->addCheckbox('phone_proposal', 'Firma oslovena telefonicky')->setDefaultValue($data['phone_proposal']);;
        $form->addCheckbox('trade_proposal', 'Nákup zboží po telefonické nabídce')->setDefaultValue($data['trade_proposal']);;

		
        $form->addFile('attachment', 'Individuální ceník:');
        $form->addHidden('filename')->setValue($data['attachment']);
        $form->addSubmit('ok', 'Uložit')->onClick[] = array($this, 'businessFormClicked');
    }

    public function businessFormClicked(SubmitButton $button) {
		
		$values = $button->getForm()->getValues();
		$updateValues = array();
		
		$file = $values['attachment'];
		
		if ($file->getName() != '') {
		
			$filename = explode('.', $file->getName());
			$fileExt = end($filename);
			$oldFilename = $values['filename'];
			$newFilename = FormatHelper::hash().'.'.$fileExt;
			
			// move uploaded file & save DB
			if (false !== $file->move(UPLOAD_DIR.'/'.$newFilename)) {
				
				// save DB
				$updateValues['attachment'] = $newFilename;
				
				// remove old file
				if (!empty($oldFilename)) unlink(UPLOAD_DIR.'/'.$oldFilename);			
			} // if move
			
		} // if filename
        
		$updateValues['phone_proposal'] = $values['phone_proposal'];
		$updateValues['trade_proposal'] = $values['trade_proposal'];
		
		if ($updateValues) dibi::query('UPDATE `companies` SET ', $updateValues, 'WHERE `companyID`=%s', $this->getParam('companyID'));


		// go back to company detail
        $this->redirect('detail', $this->getParam('companyID'));
	}
	
	public function actionDownload($companyID) {
		
		$company = dibi::select('name, attachment')->from('companies')->where('companyID = %s', $companyID)->fetch();
		
		$targetFile = UPLOAD_DIR.'/'.$company->attachment;		
		$targetName = FormatHelper::filename($company->name);
		$fileExt = @end(explode('.', $company->attachment));
		
		Environment::getHttpResponse()->setHeader('Content-type', 'application/download');
		Environment::getHttpResponse()->setHeader('Content-length', filesize($targetFile));
		Environment::getHttpResponse()->setHeader('Content-disposition', 'filename="'.iconv('utf-8', 'windows-1250', $targetName).'.'.strtolower($fileExt).'"');

		// output file content
		if (is_file($targetFile)) print file_get_contents($targetFile);
		
		$this->terminate();
	}

    public function editStudyproDataOkClicked(SubmitButton $button) {
        $args = $button->getForm()->getValues();
        $updateValues = NULL;
        if (!$args['spprofilename'] == NULL) {
            $updateValues['spprofilename'] = $args['spprofilename'];
        }
        if (!$args['spcompanyname'] == NULL) {
            $updateValues['spcompanyname'] = $args['spcompanyname'];
        }
        if (!$args['spexpiry'] == NULL) {
            $updateValues['spexpiry'] = $args['spexpiry'];

                    // jestli je Pilot nebo Running
                    
            $companyID = $this->getParam('companyID');
            $status = dibi::select('status')
                        ->from('companies')
                        ->where('companyID=%s', $companyID)
                        ->fetch();
            $status = $status['status'];

if ($status == 9){
                    // vytvoření poznámky a uložení do tabulky todo u Running
                    $ar = "";
                    $ar['created'] = $today = date('y-m-j h-i-s');
                    $ar['descr'] = 'Automaticky generovaná poznámka: Dnes končí licence!';
                    $ar['title'] = 'Dnes expiruje licence!';
                    $ar['remind'] = $args['spexpiry'];
                    $ar['whocreate'] = Environment::getUser()->getIdentity()->name;
                    $ar['status'] = 'live';
                    $ar['assigned'] = Environment::getUser()->getIdentity()->name;
                    $ar['company'] = $this->getParam('companyID');
                    dibi::insert('todo', $ar)->execute();

                    // vytvoření poznámky týden předem a uložení do tabulky todo u Running
                    $piecesDate = explode("-", $args['spexpiry']);
                    $weekBefor = (mktime (0,0,0,$piecesDate[1],$piecesDate[2],$piecesDate[0])) - (7*24*60*60);
                    $weekBefor = date("Y-m-d", $weekBefor);
                    $arOneWeekBefore = "";
                    $arOneWeekBefore['created'] = $today = date('y-m-j h-i-s');
                    $arOneWeekBefore['descr'] = 'Automaticky generovaná poznámka: Za týden bude končit licence!';
                    $arOneWeekBefore['title'] = 'Za týden bude končit licence!';
                    $arOneWeekBefore['remind'] = $weekBefor;
                    $arOneWeekBefore['whocreate'] = Environment::getUser()->getIdentity()->name;
                    $arOneWeekBefore['status'] = 'live';
                    $arOneWeekBefore['assigned'] = Environment::getUser()->getIdentity()->name;
                    $arOneWeekBefore['company'] = $this->getParam('companyID');
                    dibi::insert('todo', $arOneWeekBefore)->execute();

                    // vytvoření poznámky měsíc před koncem licence u Running
                    $piecesDate = explode("-", $args['spexpiry']);
                    $weekBefor = (mktime (0,0,0,$piecesDate[1],$piecesDate[2],$piecesDate[0])) - (4*7*24*60*60);
                    $weekBefor = date("Y-m-d", $weekBefor);
                    $arOneWeekBefore = "";
                    $arOneWeekBefore['created'] = $today = date('y-m-j h-i-s');
                    $arOneWeekBefore['descr'] = 'Automaticky generovaná poznámka: Za měsíc bude končit licence!';
                    $arOneWeekBefore['title'] = 'Za měsíc bude končit licence! Domluvit prodloužení.';
                    $arOneWeekBefore['remind'] = $weekBefor;
                    $arOneWeekBefore['whocreate'] = Environment::getUser()->getIdentity()->name;
                    $arOneWeekBefore['status'] = 'live';
                    $arOneWeekBefore['assigned'] = Environment::getUser()->getIdentity()->name;
                    $arOneWeekBefore['company'] = $this->getParam('companyID');
                    dibi::insert('todo', $arOneWeekBefore)->execute();

                    // vytvoření poznámky 3. měsíce před koncem licence u Running
                    $piecesDate = explode("-", $args['spexpiry']);
                    $weekBefor = (mktime (0,0,0,$piecesDate[1],$piecesDate[2],$piecesDate[0])) - (12*7*24*60*60);
                    $weekBefor = date("Y-m-d", $weekBefor);
                    $arOneWeekBefore = "";
                    $arOneWeekBefore['created'] = $today = date('y-m-j h-i-s');
                    $arOneWeekBefore['descr'] = 'Automaticky generovaná poznámka: Tři měsíce do konce licence!';
                    $arOneWeekBefore['title'] = 'Check and support.';
                    $arOneWeekBefore['remind'] = $weekBefor;
                    $arOneWeekBefore['whocreate'] = Environment::getUser()->getIdentity()->name;
                    $arOneWeekBefore['status'] = 'live';
                    $arOneWeekBefore['assigned'] = Environment::getUser()->getIdentity()->name;
                    $arOneWeekBefore['company'] = $this->getParam('companyID');
                    dibi::insert('todo', $arOneWeekBefore)->execute();
                    // vytvoření poznámky 6. měsíce před koncem licence u Running
                    $piecesDate = explode("-", $args['spexpiry']);
                    $weekBefor = (mktime (0,0,0,$piecesDate[1],$piecesDate[2],$piecesDate[0])) - (24*7*24*60*60);
                    $weekBefor = date("Y-m-d", $weekBefor);
                    $arOneWeekBefore = "";
                    $arOneWeekBefore['created'] = $today = date('y-m-j h-i-s');
                    $arOneWeekBefore['descr'] = 'Automaticky generovaná poznámka: Šest měsíců do konce licence!';
                    $arOneWeekBefore['title'] = 'Check and support.';
                    $arOneWeekBefore['remind'] = $weekBefor;
                    $arOneWeekBefore['whocreate'] = Environment::getUser()->getIdentity()->name;
                    $arOneWeekBefore['status'] = 'live';
                    $arOneWeekBefore['assigned'] = Environment::getUser()->getIdentity()->name;
                    $arOneWeekBefore['company'] = $this->getParam('companyID');
                    dibi::insert('todo', $arOneWeekBefore)->execute();
                    // vytvoření poznámky 9. měsíce před koncem licence u Running
                    $piecesDate = explode("-", $args['spexpiry']);
                    $weekBefor = (mktime (0,0,0,$piecesDate[1],$piecesDate[2],$piecesDate[0])) - (36*7*24*60*60);
                    $weekBefor = date("Y-m-d", $weekBefor);
                    $arOneWeekBefore = "";
                    $arOneWeekBefore['created'] = $today = date('y-m-j h-i-s');
                    $arOneWeekBefore['descr'] = 'Automaticky generovaná poznámka: Devět měsíců do konce licence!';
                    $arOneWeekBefore['title'] = 'Check and support.';
                    $arOneWeekBefore['remind'] = $weekBefor;
                    $arOneWeekBefore['whocreate'] = Environment::getUser()->getIdentity()->name;
                    $arOneWeekBefore['status'] = 'live';
                    $arOneWeekBefore['assigned'] = Environment::getUser()->getIdentity()->name;
                    $arOneWeekBefore['company'] = $this->getParam('companyID');
                    dibi::insert('todo', $arOneWeekBefore)->execute();


}

if ($status == 12){
                    // vytvoření poznámky a uložení do tabulky todo u Pilotu
                    $ar = "";
                    $ar['created'] = $today = date('y-m-j h-i-s');
                    $ar['descr'] = 'Automaticky generovaná poznámka: Dnes končí pilot!';
                    $ar['title'] = 'Dnes expiruje pilot!';
                    $ar['remind'] = $args['spexpiry'];
                    $ar['whocreate'] = Environment::getUser()->getIdentity()->name;
                    $ar['status'] = 'live';
                    $ar['assigned'] = Environment::getUser()->getIdentity()->name;
                    $ar['company'] = $this->getParam('companyID');
                    dibi::insert('todo', $ar)->execute();

                    // vytvoření poznámky týden předem a uložení do tabulky todo u Pilotu
                    $piecesDate = explode("-", $args['spexpiry']);
                    $weekBefor = (mktime (0,0,0,$piecesDate[1],$piecesDate[2],$piecesDate[0])) - (7*24*60*60);
                    $weekBefor = date("Y-m-d", $weekBefor);
                    $arOneWeekBefore = "";
                    $arOneWeekBefore['created'] = $today = date('y-m-j h-i-s');
                    $arOneWeekBefore['descr'] = 'Automaticky generovaná poznámka: Za týden bude končit pilotní projekt!';
                    $arOneWeekBefore['title'] = 'Za týden bude končit pilotní projekt! Je potřeba zaslat ceník a domluvit případnou další spolupráci jako placení klienti.';
                    $arOneWeekBefore['remind'] = $weekBefor;
                    $arOneWeekBefore['whocreate'] = Environment::getUser()->getIdentity()->name;
                    $arOneWeekBefore['status'] = 'live';
                    $arOneWeekBefore['assigned'] = Environment::getUser()->getIdentity()->name;
                    $arOneWeekBefore['company'] = $this->getParam('companyID');
                    dibi::insert('todo', $arOneWeekBefore)->execute();

                    // vytvoření poznámky 3.týdnyny předem a uložení do tabulky todo u Pilotu
                    $piecesDate = explode("-", $args['spexpiry']);
                    $weekBefor = (mktime (0,0,0,$piecesDate[1],$piecesDate[2],$piecesDate[0])) - (21*24*60*60);
                    $weekBefor = date("Y-m-d", $weekBefor);
                    $arOneWeekBefore = "";
                    $arOneWeekBefore['created'] = $today = date('y-m-j h-i-s');
                    $arOneWeekBefore['descr'] = 'Automaticky generovaná poznámka: Tři týdny do konce Pilotu!';
                    $arOneWeekBefore['title'] = 'Tři týdny do konce pilotu, zkontrolovat a podpora.';
                    $arOneWeekBefore['remind'] = $weekBefor;
                    $arOneWeekBefore['whocreate'] = Environment::getUser()->getIdentity()->name;
                    $arOneWeekBefore['status'] = 'live';
                    $arOneWeekBefore['assigned'] = Environment::getUser()->getIdentity()->name;
                    $arOneWeekBefore['company'] = $this->getParam('companyID');
                    dibi::insert('todo', $arOneWeekBefore)->execute();
}
        }
        if (!$args['HRlogin'] == NULL) {
            $updateValues['HRlogin'] = $args['HRlogin'];
        }
        if (!$args['HRpassword'] == NULL) {
            $updateValues['HRpassword'] = $args['HRpassword'];
        }
        if (!$args['testlogin'] == NULL) {
            $updateValues['testlogin'] = $args['testlogin'];
        }
        if (!$args['testpassword'] == NULL) {
            $updateValues['testpassword'] = $args['testpassword'];
        }
        //Debug::dump($args);Exit;
        if ($updateValues) {
            dibi::query('UPDATE `companies` SET ', $updateValues, 'WHERE `companyID`=%s', $this->getParam('companyID'));
        }
        $this->redirect('detail', $this->getParam('companyID'));
    }

    protected function createComponentEditProductsForm($name) {
        
		$form = new AppForm($this, $name);

		$data = dibi::select('id, name')
                  ->from('products')
                  ->where('active')
				  ->groupBy('name')
                  ->fetchPairs();


        $form->addGroup();
        $form->addSelect('product', 'Vyberte produkt:', $data);//->setDefaultValue($data['notes']);
        $form->addSubmit('ok', 'Přidat produkt')->onClick[] = array($this, 'editProductsClicked');
    }
	
	public function editProductsClicked(SubmitButton $button) {
		$values = $button->getForm()->getValues();
        
		dibi::query('REPLACE INTO `product_offers` (companyID, productID) VALUES (%s, %i)', $this->getParam('companyID'), $values['product']);
        
		$this->redirect('detail', $this->getParam('companyID'));
	}

	public function actionDeleteProduct($company, $product) {
        
		dibi::query('DELETE FROM `product_offers` WHERE companyID = %s AND productID = %i LIMIT 1', $company, $product);
        
		$this->redirect('detail', $company);
	}

    public function editStatusOkClicked(SubmitButton $button) {
        $args = $button->getForm()->getValues();
        $updateValues = NULL;
        if (!$args['status'] == NULL) {
            $updateValues['status'] = $args['status'];
        }
        if ($updateValues) {
            dibi::query('UPDATE `companies` SET ', $updateValues, 'WHERE `companyID`=%s', $this->getParam('companyID'));
        }
        $this->redirect('detail', $this->getParam('companyID'));
    }

    protected function createComponentEditStatusForm($name) {
        $form = new AppForm($this, $name);

        $rows = dibi::select('*')
                        ->from('status')
                        ->fetchAll();

        foreach ($rows as $row) {
            $statuses[$row['statusID']] = $row['statusName'];
        }
        $form->addRadioList('status', 'Status', $statuses);

        $form->addProtection('Vypršel ochranný časový limit, odešlete prosím formulář ještě jednou');
        $form->addSubmit('ok', 'Změnit status')->onClick[] = array($this, 'editStatusOkClicked');
    }

    public function handleExportXML($companyID) {

        // natahnutí dat z DB
        $row = dibi::select('*')
                        ->from('companies')
                        ->where('companyID=%s', $companyID)
                        ->fetch();
        //debug::dump($row);Exit;
        // export do CSV
        $filename = 'bufo_company.csv';
        // zformátovat a předat data z $row
        $csv_terminated = "\n";
        $csv_separator = ";";

        $file =
                "Company" . $csv_separator .
                "Street" . $csv_separator .
                "City" . $csv_separator .
                "Postal Code" . $csv_separator .
                "Country" . $csv_separator .
                "Fax" . $csv_separator .
                "Web" . $csv_terminated;


        $file .=
                $row['name'] . $csv_separator .
                $row['adresStreet'] . $csv_separator .
                $row['adresCity'] . $csv_separator .
                $row['adresPostalcode'] . $csv_separator .
                $row['country'] . $csv_separator .
                $row['fax'] . $csv_separator .
                $row['web'] . $csv_terminated;

        $response = Environment::getHttpResponse();
        $response->setHeader('Content-Description', 'File Transfer');
        $response->setContentType('text/csv', 'UTF-8');
        $response->setContentType('application/csv', 'UTF-8');
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $filename);
        $response->setHeader('Content-Transfer-Encoding', 'binary');
        $response->setHeader('Expires', 0);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
        $response->setHeader('Pragma', 'public');
        $response->setHeader('Content-Length', strlen($file));
        ob_clean();
        flush();
        echo $file;
        $this->terminate();
    }

    public function historyOkClicked(SubmitButton $button) {
        $values = $button->getForm()->getValues();
        $updateValues = array('history' => $values['text']);

        dibi::query('UPDATE `companies` SET ', $updateValues, 'WHERE `companyID`=%s', $this->getParam('companyID'));

        $this->redirect('detail', $this->getParam('companyID'));
    }

    protected function createComponentHistoryForm($name) {
        $companyID = $this->getParam('companyID');
        $row = dibi::select('history')
                        ->from('companies')
                        ->where('companyID=%s', $companyID)
                        ->fetch();

        $form = new AppForm($this, $name);
        $form->addTextArea('text')->setEmptyValue($row['history'])->getControlPrototype()->class('mceEditor');
        $form->addProtection('Vypršel ochranný časový limit, odešlete prosím formulář ještě jednou');
        $form->getElementPrototype()->onsubmit('tinyMCE.triggerSave()');
        $form->addSubmit('ok', 'Odeslat')->onClick[] = array($this, 'historyOkClicked');
    }

    public function addTaskOkClicked(SubmitButton $button) {
        $args = $button->getForm()->getValues();
        //Debug::dump($args);Exit;
        $args['created'] = $today = date('y-m-j h-i-s');
        $args['whocreate'] = Environment::getUser()->getIdentity()->name;
        $args['status'] = 'live';
        $args['assigned'] = Environment::getUser()->getIdentity()->name;
        $args['company'] = $this->getParam('companyID');

        dibi::insert('todo', $args)->execute();

        $this->redirect('detail', $this->getParam('companyID'));
    }

    protected function createComponentAddTaskForm($name) {
        $form = new AppForm($this, $name);

        $form->addText('remind', 'Datum:')->getControlPrototype()->class('datepicker')
                ->addRule(Form::FILLED, 'Prosím, uveďte datum.');
        $form->addText('title', 'Název:', 100)
                ->addRule(Form::FILLED, 'Prosím, uveďte název úkolu.');
        $form->addTextArea('descr', 'Popis', 98, 5);

        $form->addProtection('Vypršel ochranný časový limit, odešlete prosím formulář ještě jednou');
        $form->addSubmit('ok', 'Přidat')->onClick[] = array($this, 'addTaskOkClicked');
    }

    function deleteItem($companyID) {
        // smazani
        //Debug::dump('Smazáno:' . $companyID);Exit;
		
		$companyModel = new CompanyModel();
		
		return $companyModel->update($companyID, array('deleted' => 1));
/*
        dibi::delete('*')
                ->from('companies')
                ->where('companyID=%s', $companyID)
                ->execute();
*/
    }

    function createComponentConfirmForm() {
        $form = new ConfirmationDialog();
        $form
                ->addConfirmer(
                        'delete', // název signálu bude 'confirmDelete!'
                        array($this, 'deleteItem'), // callback na funkci při kliku na YES
                        'Opravdu smazat?' // otázka (může být i callback vracející string)
        );
        $form->getFormElementPrototype()->addClass('ajax');
        return $form;
    }

    function handleChangeStatus($id) {
        // smazani
        //Debug::dump('Change todo ID:' . $id);Exit;

        $task = dibi::select('*')
                        ->from('todo')
                        ->where('todoID=%i', $id)
                        ->fetch();
        $old = $task['status'];

        if ($old == 'live') {
            $new = 'done';
        } else {
            $new = 'live';
        }
        $updateValues['status'] = $new;
        dibi::query('UPDATE `todo` SET ', $updateValues, 'WHERE `todoID`=%i', $id);

        $this->redirect('detail', $this->getParam('companyID'));
    }

    function handleRemoveTodo($todoID) {
        dibi::delete('*')
                ->from('todo')
                ->where('todoID=%i', $todoID)
                ->execute();
        $this->redirect('detail', $this->getParam('companyID'));
    }

    function handleDeleteLogo($logoID) {
        $updateValues = NULL;
        $updateValues['logo'] = NULL;
        dibi::query('UPDATE `companies` SET ', $updateValues, 'WHERE `companyID`=%s', $logoID);
        $this->redirect('detail', $logoID);
    }

    public function uploadLogoOkClicked(SubmitButton $button) {
        $args = $button->getForm()->getValues();
        $updateValues = NULL;
        if (!$args['path'] == NULL) {
            $image = Image::fromFile($args['path']);
            $imageName = $this->getParam('companyID') . "_" . $args['path']->name;
            $image->resize(230, 80, Image::ENLARGE | Image::STRETCH);
            $image->save('images/logos/' . $imageName);
            $updateValues['logo'] = $imageName;
        }
        if ($updateValues) {
            dibi::query('UPDATE `companies` SET ', $updateValues, 'WHERE `companyID`=%s', $this->getParam('companyID'));
        }
        $this->redirect('detail', $this->getParam('companyID'));
    }

    protected function createComponentUploadLogoForm($name) {
        $form = new AppForm($this, $name);

        $form->addFile('path', 'Path to file:')
                ->addRule(Form::FILLED, 'Path must be fill!')
                ->addRule(Form::MIME_TYPE, 'Allow Image formats is .jpg .gif and .png', 'image/jpeg,image/gif,image/png')
                ->addRule(Form::MAX_FILE_SIZE, 'Image size is big, Maximum is 0.5 MB', '500000');
        $form->addSubmit('ok', 'Upload')->onClick[] = array($this, 'uploadLogoOkClicked');
    }

}