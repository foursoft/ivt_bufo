<?php

	final class Dealer_ProductPresenter extends Dealer_BasePresenter {


		protected function createComponentProductGrid() {
			
			
			$model = new ProductModel();
			$grid = new DataGrid;	
	
			$grid->itemsPerPage = 50;
			$grid->rememberState = TRUE;
			$grid->bindDataTable($model->getDataSource());
			$grid->multiOrder = FALSE; // order by one column only
	
			$grid->keyName = 'product';
	
			/* add some columns */
			$grid->addColumn('name', 'Produkt')->getHeaderPrototype()->addStyle('width: 430px');
			$grid->addColumn('code', 'Kódy / šarže produktu')->getHeaderPrototype()->addStyle('width: 400px');

			$grid->addColumn('product', 'Možnosti');
	
	
			/* add some filters */
	
			$grid['name']->addFilter();
			$grid['code']->addFilter();
			
			$grid['product']->formatCallback[] = array($this, 'ProductLink');

			// nadefinujeme si operace, tyto hodnoty je možno nechat překládat translatorem
			$operations = array('lookup' => 'kdo koupil', 'addOffer' => 'produkt do nabídky');
			
			// poté callback(y), které mají operaci zpracovat
			$callback = array($this, 'productLookup'); // $this je presenter
			
			// povolíme operace
			$grid->allowOperations($operations, $callback, 'product');
	
/*	
			$grid->addActionColumn('Akce')->getHeaderPrototype()->addStyle('width: 98px');
			$icon = Html::el('span');
			$grid->addAction('Detail', 'Companies:detail', clone $icon->class('icon icon-detail'));
			$grid->addAction('Smazat', 'confirmForm:confirmDelete!', clone $icon->class('icon icon-del'), true);
*/	
			return $grid;
		}
		
		public function productLookup(SubmitButton $input) {
			
			$values = (object) $input->getForm()->getValues();
			$products = implode(',', array_keys(array_filter($values->checker, function($value) { return (boolean) $value; })));

			switch ($values->operations) {
				
				case 'lookup' : 
				 $this->getPresenter()->redirect('Companies:default', array('product' => $products));
				break;

				case 'addOffer' :
					$model = new ProductModel();
					$model->setOffer($products);
					$this->getPresenter()->redirect('Product:default');
				break;
				
			}
			
		}
		
		
		public function ProductLink($value) {
			return Html::el('a', 'Kdo koupil?')->href('/dealer/companies/?product='.$value);
		}
		

	}

?>