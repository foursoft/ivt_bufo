<?php



// Step 1: Load Nette Framework
// this allows load Nette Framework classes automatically so that
// you don't have to litter your code with 'require' statements
// require LIBS_DIR . '/Nette/loader.php';
require LIBS_DIR . '/Nette/loader.php';


if (is_file(APP_DIR . '/extensions.php')) {
	include_once APP_DIR . '/extensions.php';
}


// Step 2: Configure environment
// 2a) enable Nette\Debug for better exception and error visualisation
Debug::enable();

// 2b) load configuration from config.ini file
Environment::loadConfig();



// Step 3: Configure application
$application = Environment::getApplication();

// Step 3a: Configure database connection
dibi::connect(Environment::getConfig('database'));


DataGrid::extensionMethod('addDateRangeColumn', function (DataGrid $_this, $name, $caption, $format = '%x') {
	return $_this[$name] = new DateRangeColumn($caption, $format);
});


//$application->onStartup[] = 'Services::initialize';
$application->onStartup[] = 'BaseModel::initialize';
$application->onShutdown[] = 'BaseModel::disconnect';

// Step 4: Setup application router
$router = $application->getRouter();

// mod_rewrite detection
if (function_exists('apache_get_modules') && in_array('mod_rewrite', apache_get_modules())) {
    # AdminModule routes
    $router[] = new Route('<module>/<presenter>/<action>/<id>', array(
        'module'    => 'Dealer',
        'presenter' => 'Default',
        'action'    => 'default',
        'id'        => null
    ));

    $router[] = new Route('index.php', array(
		'module' => 'Dealer',
		'presenter' => 'Default',
	), Route::ONE_WAY);

} else {
	$router[] = new SimpleRouter('Dealer:Default:default');
}

// Step 5: Run the application!
$application->run();
