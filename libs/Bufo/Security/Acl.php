<?php

class Bufo_Security_Acl extends Permission
{
    public function __construct()
    {
        // roles
        $this->addRole('dealer');
        $this->addRole('countryboss');

        // resources
        $this->addResource('Dealer_SetupPresenter');
        $this->addResource('Dealer_DefaultPresenter');
        $this->addResource('Dealer_TaskPresenter');
        $this->addResource('Dealer_InfoPresenter');
        $this->addResource('Dealer_DocsPresenter');
        $this->addResource('Dealer_CompaniesPresenter');

        $this->addResource('Dealer_BossPresenter');

        // privileges
        $this->allow('dealer',  'Dealer_SetupPresenter', Permission::ALL);
        $this->allow('dealer',  'Dealer_DefaultPresenter', Permission::ALL);
        $this->allow('dealer',  'Dealer_TaskPresenter', Permission::ALL);
        $this->allow('dealer',  'Dealer_InfoPresenter', Permission::ALL);
        $this->allow('dealer',  'Dealer_DocsPresenter', Permission::ALL);
        $this->allow('dealer',  'Dealer_CompaniesPresenter', Permission::ALL);

        $this->allow('countryboss',  'Dealer_BossPresenter', Permission::ALL);
        $this->allow('countryboss',  'Dealer_SetupPresenter', Permission::ALL);
        $this->allow('countryboss',  'Dealer_DefaultPresenter', array('logout'));

        }
}
