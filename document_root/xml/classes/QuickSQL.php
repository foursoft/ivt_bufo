<?php

	class QuickSQL extends SQL {
		
		protected $connection;
		
		
		public function connect($_dbserver, $_dbusername, $_dbpassword, $_dbname, $_charset = '') {
			$this->connection = new SQLconnection($_dbserver, $_dbusername, $_dbpassword, $_dbname, $_charset);
			return $this->connection->Connect();
		}
		
	}

	/** Databázové SQL připojení. */
	class SQLconnection {

		protected $dbserver;
		protected $dbusername;
		protected $dbpassword;
		protected $dbname;
		protected $charset;
		protected $error;
		protected $errorlog;
		protected $errordetails;
		protected $sql;
		
		public $db;
		public $connected;
		public $errcode;
		public $createlog = false;
		
		
		public function SQLconnection($_dbserver, $_dbusername, $_dbpassword, $_dbname, $_charset = '') {
		
			$this->dbserver = $_dbserver;
			$this->dbusername = $_dbusername;
			$this->dbpassword = $_dbpassword;
			$this->dbname = $_dbname;
			$this->charset = $_charset;
			
			$this->sql = new SQL();

			if ($this->createlog):
				$this->errorlog = new Log('error.log', 'errorlog.txt');
				$this->errordetails = array(
					'timestamp' => date('d.m.Y-H:i:s'),
					'event' => $_REQUEST['event'],
					'filename' => basename($_SERVER['PHP_SELF']),
					'request' => print_r($_REQUEST, true),
				);
			endif;

		}

		public function Connect() {
		
			// connection failed //
			if (false === ($this->db = @mysql_connect($this->dbserver, $this->dbusername, $this->dbpassword))):
				$this->errcode = 402;
				$this->error = 'Nelze se připojit k serveru.';
				$this->connected = false;

				$this->errordetails['errcode'] = $this->errcode;
				$this->errordetails['sqlquery'] = 'Connect';
				$this->errordetails['dberror'] = $this->ShowError();
				if ($this->createlog) $this->errorlog->Write($this->errordetails);;

				else:
					// db selection failed //
					if (false === @mysql_select_db($this->dbname, $this->db)):
						$this->errcode = 403;
						$this->error = 'Nelze vybrat databázi.';
						$this->connected = false;

						$this->errordetails['errcode'] = $this->errcode;
						$this->errordetails['sqlquery'] = 'Select DB';
						$this->errordetails['dberror'] = $this->ShowError();
						if ($this->createlog) $this->errorlog->Write($this->errordetails);;

						// connection successfull //
						else: 
							if (!empty($this->charset)) $this->sql->ExecSQL('SET NAMES %s', $this->charset);
							$this->connected = true;
					endif;
				endif;
			return $this->connected;			
		}

		public function Close() {
			mysql_close($this->db);
		}
		
		public function ShowError() {
			return $this->error;
		}
		
	}



	class SQL {

		protected $queryString;
		protected $resource;
		protected $result;
		protected $limits;
		protected $itemsPerPage = 0;
		protected $error;
		protected $errlog;
		protected $errloglist;
		protected $errdetails;
		protected $data;
		protected $listData;
		protected $formatStr;
		
		public $createlog = true;
		public $status;


		public static function instance() {
			return new SQL();
		}
		
		public function SQL() {
			$this->Init();
		}
		
		protected function LogError() {
			$this->errdetails['sqlquery'] = $this->queryString;
			$this->errdetails['dberror'] = $this->sqlError();
			$this->errorlog->Write($this->errdetails);
		}
		
		/** Inicializace a nastavení třídy. */
		public function Init() {

			$this->errloglist = array();
			
			if (!class_exists('Log')) $this->createlog = false;
			
			if ($this->createlog):
				$this->errorlog = new Log('error.log', 'errorlog.txt');
				$this->errdetails = array(
					'errcode' => 401,
					'timestamp' => gmdate('d.m.Y - H:i:s'),
					'event' => $_REQUEST['event'],
					'filename' => basename($_SERVER['PHP_SELF']),
				 'uri' => $_SERVER['REQUEST_URI'],
					'request' => print_r($_REQUEST, true),
				);
			endif;

			$this->SetLimit($this->itemsPerPage);	
			$this->formatStr = new SQLFormatStr();	
		}

		
			/** Provede MySQL dotaz bez návratu hodnot (mysql_unbuffered_query). */
			public function ExecSQL($_query, $_args = NULL) {

			$_args = (is_array($_args) and func_num_args() == 2) ?	$_args : array_slice(func_get_args(), 1);			
			$this->queryString = !empty($_args) ? $this->formatStr->Format($_query, $_args) : $_query;
			
			//trace($this->queryString);
			
			if ($this->queryString != '') $this->status = mysql_unbuffered_query($this->queryString);
			
			//print nl2br($this->queryString).'<hr>';
			
			if (($this->error = mysql_error()) !== ''):
			
				if ($this->createlog) $this->LogError();
			 return false;
				 
			else: 
				return $this->AffectedRows();
			endif;
		}

		/** Provede MySQL dotaz. */
		public function Query($_query, $_args = NULL) {
			
			if (empty($_query)) return $this;

			$_args = (is_array($_args) and func_num_args() == 2) ?	$_args : array_slice(func_get_args(), 1);
			$this->queryString = !empty($_args) ? $this->formatStr->Format($_query, $_args) : $_query;
			
			if (!empty($this->limits) and strpos($this->queryString, 'SELECT') !== false and strpos($this->queryString, 'LIMIT') === false) $this->queryString.= $this->limits;

			$this->resource = mysql_query($this->queryString);
			$this->status = $this->resource !== false ? true : false;
			
			if (($this->error = mysql_error()) !== ''): 

				if ($this->createlog) $this->LogError();
				return $this;//return false;
				
			else:
				return $this; //return $this->AffectedRows();
			endif;

		}

		/** Parsuje MySQL dotaz a vrátí ho v textové podobě tak, jak by byl poslán do databáze. */
		public function Test($_query, $_args = NULL) {
			
			if (empty($_query)) return '';

			$_args = (is_array($_args) and func_num_args() == 2) ?	$_args : array_slice(func_get_args(), 1);
			$this->queryString = !empty($_args) ? $this->formatStr->Format($_query, $_args) : $_query;
			
			if (!empty($this->limits) and strpos($this->queryString, 'SELECT') !== false and strpos($this->queryString, 'LIMIT') === false) $this->queryString.= $this->limits;

			print '<hr /><b>'.nl2br($this->queryString).'</b><hr />'; 
			die;
			
		}

		/** Alias metody Query(). */
		public function Select($_query, $_args = NULL) {
			$_args = is_array($_args) ?	$_args : array_slice(func_get_args(), 1);			
			return $this->Query($_query, $_args);
		}

		/** Provede MySQL dotaz na jediný řádek v tabulce a ihned vrátí výsledek. --- ZASTARALÁ FUNKCE | náhrada: -> Select('query')->Fetch()*/
		public function SelectOne($_query) {
			$_args = func_get_args();
			$_result_type = $_args[1];
					
			$this->setLimit(1);
			$this->Query($_query);
								
					switch ($_result_type):
					
						case MYSQL_ASSOC :
							$_result = (false !== ($_output = $this->FetchAssoc())) ? $_output : false;
						break;

						case MYSQL_NUM :
							$_result = (false !== ($_output = $this->FetchArray())) ? $_output : false;
						break;
						
						default : $_result = (false !== ($_output = $this->FetchObj())) ? $_output : false;
						
					endswitch;
					
					return $_result;

		}
		
		/** Vrátí struktoru databázové tabulky $table jako pole array. Pokud je parametr full nastaven na TRUE, metoda vrátí maximum informací o tabulce. Výčet jednotlivých sloupců lze omezit parametrem showcolumns. Pokudje parametr $fullkeys nastaven na TRUE, jako klíče polí budou obsahovat i název tabulky (table1:column2). */
		public function GetColumns($_table, $_full = false, $_showcolumns = array(), $_fullKeys = false) {
			
			if (false === $this->Query('SHOW COLUMNS FROM `'.$_table.'`')) return array();
			$_columns = array();

			
			if ($_full):
				while (false !== ($_field = $this->FetchObj())):
					if (count($_showcolumns) > 0 and !in_array($_field->Field, $_showcolumns)) continue;
					$_key = ($_fullKeys) ? $_table.':'.$_field->Field : $_field->Field;
					$_columns[$_key] = (object) null;
					$_columns[$_key]->name = $_field->Field;
					$_columns[$_key]->type = strtoupper(substr($_field->Type.'(', 0, strpos($_field->Type.'(', '(')));
					$_columns[$_key]->length = preg_match('/\((\d*)|,?\)/', $_field->Type , $_length) > 0 ? (int) end($_length) : 0; //(false === strpos($_field->Type, '(')) ? NULL : substr($_field->Type, strpos($_field->Type, '(')+1, strpos($_field->Type, ')')-strpos($_field->Type, '(')-1);
					$_columns[$_key]->null = ($_field->Null == 'YES') ? true : false;
					$_columns[$_key]->primary = ($_field->Key == 'PRI') ? true : false;
					$_columns[$_key]->index = ($_field->Key == 'MUL') ? true : false;
					$_columns[$_key]->unique = ($_field->Key == 'UNI') ? true : false;
					$_columns[$_key]->default = $_field->Default;

				endwhile;
			else:	
				while (false !== ($_field = $this->FetchObj())) $_columns[$_field->Field] = strtoupper(substr($_field->Type.'(', 0, strpos($_field->Type.'(', '(')));
			endif;
									
			return $_columns;
		}
		
		/** Vrátí první pole výsledku MySQL dotazu. --- ZASTARALÁ FUNKCE | náhrada: -> Select('query')->FetchSingle() */
		public function GetResult($_query, $_args = NULL) {

			$_args = is_array($_args) ?	$_args : array_slice(func_get_args(), 1);
			$this->Query($_query, $_args);

			return ($this->RecordsFound() > 0) ? $this->FetchSingle() : false;					
		}

		/** Nastaví atomatický limit pro počet vrácených výsledků v MySQL dotazu. */
		public function SetLimit() {
			$_args = func_get_args();
			
			if ($_args[0] == 0 and !isset($_args[1])) { $this->limits = ''; return true; }
			
			if (count($_args) > 0):
			
				if (is_array($_args[0])) {
				
					if (isset($_args[0][0])) $_offset = abs($_args[0]); else $_offset = 0;
					if (isset($_args[0][1])) $this->itemsPerPage = abs($_args[1]); else { $_offset = 0; $this->itemsPerPage = abs($_args[0]); }
					
				 } else {
				
					if (isset($_args[0])) $_offset = abs($_args[0]); else $_offset = 0;
					if (isset($_args[1])) $this->itemsPerPage = abs($_args[1]); else { $_offset = 0; $this->itemsPerPage = abs($_args[0]); }
				
				}
				
				$this->limits = " LIMIT $_offset, $this->itemsPerPage";
				
				return true;
				
			endif;
		}

		public function FetchObj() {
			$this->data = (is_resource($this->resource)) ? mysql_fetch_object($this->resource) : false; 
			return $this->data;
		}

		public function Fetch() {
			return $this->FetchObj();
		}

		public function FetchAssoc() {
			$this->data = (is_resource($this->resource)) ? mysql_fetch_assoc($this->resource) : false;
			return $this->data;
		}

		public function FetchArray() {
			$this->data = (is_resource($this->resource)) ? mysql_fetch_array($this->resource, MYSQL_NUM) : false;
			return $this->data;
		}
		
		public function FetchSingle() {
			return is_array($_result = $this->FetchArray()) ? current($_result) : false;
		}

		/** Alias metody contents2object(). */
		public function FetchAll($_keyindex = NULL) {
			return $this->contents2object($_keyindex);
		}

		public function contents2object($_keyindex = NULL) {
		
			$_contents = array();

			if ($this->sqlError() == ''):
				while (false !== ($_data = $this->FetchObj())) if ($_keyindex != NULL and !empty($_data->$_keyindex)) $_contents[$_data->$_keyindex] = $_data; else $_contents[] = $_data;
				
				return $_contents;
			else:
				$this->LogError();
				return $_contents;
			endif;

		}
		
		public function contents2assoc($_keyindex = NULL) {

			$_contents = array();
			
			if ($this->sqlError() == ''):
				while (false !== ($_data = $this->FetchAssoc())) if ($_keyindex != NULL and !empty($_data->$_keyindex)) $_contents[$_data->$_keyindex] = $_data; else $_contents[] = $_data;
				
				return $_contents;
			else:
				$this->LogError();
				return $_contents;
			endif;
		}
	
		public function contents2array($_keyindex = NULL) {

			$_contents = array();

			if ($this->sqlError() == ''):
				while (false !== ($_data = $this->FetchArray())) if ($_keyindex != NULL and !empty($_data->$_keyindex)) $_contents[$_data->$_keyindex] = $_data; else $_contents[] = $_data;
				
				return $_contents;
			else:
				$this->LogError();
				return $_contents;
			endif;
		}
	
		public function contents2list() {

			$_contents = array();
			
			while (false !== ($_data = mysql_fetch_assoc($this->resource))):
				if (count($_data) == 1) $_contents[] = trim(current($_data));
				else foreach($_data as $_key => $_value) $_contents[$_key][] = $_value;
			endwhile;
			
			return $_contents;
		}
		
		public function contents2xml($_listNode = 'items', $_rootNode = 'xml', $_cdata = true, $_node2lettercase = NULL) {

			if ($this->sqlError() == ''):
				$_contents = '';
				while (false !== ($_data = $this->FetchAssoc())):
					$_node = '';
					foreach ($_data as $_tagname => $_value):
						if ($_node2lettercase != NULL) $_tagname = ($_node2lettercase == CASE_UPPER) ? strtoupper($_tagname) : strtolower($_tagname);
						$_node.= "\t<$_tagname>".($_cdata ? "<![CDATA[$_value]]>" : $_value)."</$_tagname>\n";
					endforeach;
					$_contents.= "<$_listNode>\n$_node</$_listNode>\n";
				endwhile;
				
				return "<$_rootNode>\n$_contents</$_rootNode>";
			else:
				$this->LogError();
				return false;
			endif;
			
		}

		public function keys2values($_query = NULL) {
			if ($_query !== NULL)
				if (false === $this->Select($_query)) return false;
		
				$_contents = array();
				while (false !== ($_data = mysql_fetch_array($this->resource, MYSQL_NUM))) { $_contents[$_data[0]] = $_data[1]; }

				return $_contents;
		}
		
		/** Alias keys2values. */
		public function FetchPairs($_query = NULL) {
			return $this->keys2values($_query);
		}
		
		public function RecordsTotal() {
			return $this->Select('SELECT FOUND_ROWS()')->FetchSingle();
		}
		
		public function RecordsFound() {
			return mysql_num_rows($this->resource);
		}
		
		public function AffectedRows() {
			return mysql_affected_rows();
		}

		public function SqlError() {
			return $this->error;
		}

		public function SqlErrorNo() {
			return mysql_errno();
		}
		
		public function ErrorLog() {
			return $this->errloglist;
		}
		
		public function LastInsertId() {
			return mysql_insert_id();
		}

		/** Vypne režim AUTOCOMMIT a zavolá SQL příkaz START TRANSACTION. */
		public function Begin() {
		 if (false === $this->ExecSQL('SET AUTOCOMMIT=0')) return false;
			return $this->ExecSQL('START TRANSACTION');
		}
		
		/** Zavolá SQL příkaz COMMIT a zapne režim AUTOCOMMIT. */
		public function Commit() {
		 if (false === $this->ExecSQL('COMMIT')) return false;
			$this->ExecSQL('SET AUTOCOMMIT=1');

			return true;
		}
		
		/** Zavolá SQL příkaz ROLLBACK a zapne režim AUTOCOMMIT. */
		public function Rollback() {
		 if (false === $this->ExecSQL('ROLLBACK')) return false;
			$this->ExecSQL('SET AUTOCOMMIT=1');
		}
	
	}

	class SQLFormatStr {
	
		/** pracovní proměnná - obsahuje senzma parametrů při volání metody Add */
		protected $match2replace;
	
	
		/** Formátuje vstupní řetězec metody Add. */
		protected function makeFormat($_match) {
			
			$_modifier = strtolower(current($_match));
			$_arg = each($this->match2replace);
			$_input = !in_array($_modifier, array('%a', '%k')) ? current($_arg) : (is_array(current($_arg)) ? current($_arg) : $this->match2replace);
			
			switch ($_modifier) {
				case '%d' : 
				case '%i' : $_output = (int) $_input; break; // integer
				case '%u' : $_output = (int) abs($_input); break; // unsigned integer
				case '%f' : $_output = (float) str_replace(',', '.', $_input); break; // float
				case '%c' : $_input = substr((string) $_input, 0, 1); // single character
				case '%s' : $_output = (string) "'".addslashes($_input)."'"; break; // string
				case '%t' : $_output = (string) addslashes($_input); break; // unquoted string
				case '%a' : $_output = (string) ((is_array($_input) and !empty($_input)) ? "'".implode("', '", $_input)."'" : "''"); break; // array
				case '%k' : $_output = (string) ((is_array($_input) and !empty($_input)) ? implode(', ', $_input) : 0); break; // unquoted array
				
				default : $_output = $_modifier; // leave unchanged
			}
			
			return $_output;
		}
		
		
		/** Provede zformátování řetězce ze vstupního parametru $input. Parametr $args obsahuje parametry, kterými se má řetězec nahradit pomocí modifikátorů. */
		public function Format($_input, $_args) {
			
			if (!empty($_input)/* and !empty($_args)*/) {
				$this->match2replace = (array) $_args;
				$_input = preg_replace_callback('/%[idufstcak]\b/i', array($this, 'makeFormat'), $_input);
				unset($this->match2replace);
			} // endif
			
			return $_input;
			
		}

	}

?>